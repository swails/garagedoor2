pipeline {
    agent none

    options {
        buildDiscarder logRotator(numToKeepStr: '10')
    }

    post {
        // GitLab updates seem to work for BitBucket :)
        failure {
            updateGitlabCommitStatus name: 'jenkins', state: 'failed'
        }
        aborted {
            updateGitlabCommitStatus name: 'jenkins', state: 'failed'
        }
        success {
            updateGitlabCommitStatus name: 'jenkins', state: 'success'
        }
    }

    stages {
        stage("Run tests") {
            agent { dockerfile { filename 'Dockerfile.test' } }
            steps {
                sh "pylint --load-plugins=pylint_django users garagedoor garagedoor2 integration_tests"
                sh "coverage run --branch -m manage test garagedoor users"
                sh "coverage report -m --skip-covered --omit=garagedoor2/settings.py,manage.py"
                sh "coverage xml --omit=garagedoor2/settings.py,manage.py -o coverage.xml"
                // Now publish the coverage report
                cobertura autoUpdateHealth: false,
                          autoUpdateStability: false,
                          coberturaReportFile: 'coverage.xml',
                          conditionalCoverageTargets: '95, 90, 95',
                          lineCoverageTargets: '95, 90, 95',
                          maxNumberOfBuilds: 0,
                          methodCoverageTargets: '98, 95, 98',
                          onlyStable: false,
                          sourceEncoding: 'ASCII',
                          zoomCoverageChart: false
            }
        }

        stage("Build Docker Image") {
            agent { label "docker && linux" }

            when {
                branch "master"
            }

            steps {
                script {
                    def image = docker.build("swails/garagedoor:latest")
                    docker.withRegistry("", "swails-dockerhub") {
                        image.push("latest")
                    }
                }
            }
        }
    }
}
