#!/bin/sh -e
# Execute database migrations (if any exist)
python manage.py migrate
service nginx start
gunicorn -w 4 -b 0:8080 --max-requests=100 garagedoor2.wsgi:application
