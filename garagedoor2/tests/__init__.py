""" Test framework for all apps inside this project """

from users.models import User
from django.test import TestCase

class TestCaseWithUser(TestCase):
    """ Test case with the ability to create users """

    def create_user(self, username, password, first_name='', last_name='',
                    myq_username='', myq_password=''):
        """ Creates a user and saves it to the database, and returns that user """
        user = User.objects.create_user(username, password=password, first_name=first_name,
                                        last_name=last_name)
        user.myq_username = myq_username
        user.myq_password = myq_password
        user.save()
        return user
