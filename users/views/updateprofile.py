""" View for updating account information """
import logging

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.urls import reverse_lazy
from django.views.generic.edit import FormView

from ..forms import UpdateProfileForm

LOGGER = logging.getLogger(__name__)

@method_decorator(login_required, name='dispatch')
class UpdateProfile(FormView):
    """ View for updating profile information """
    template_name = 'registration/update_profile.html'
    form_class = UpdateProfileForm
    success_url = reverse_lazy('users:update_profile')

    def form_valid(self, form):
        """ If the form is valid, update the user """
        self._update_user(form, self.request.user)
        return super().form_valid(form)

    def get_initial(self):
        """ Fill the initial form fields from the current values in the account """
        # The "get()" method needs to set the "user" attribute here
        user = self.request.user
        LOGGER.debug('user is %s', user)
        LOGGER.debug('email is %s', user.email)
        name = ((user.first_name or '') + ' ' + (user.last_name or '')).strip()
        return dict(
            myq_username=user.myq_username or '',
            myq_password=user.myq_password or '',
            name=name,
            email=user.email or '',
        )

    def _update_user(self, form, user):
        """ Updates a user object from information provided in a form """
        if form.data.get('myq_username', None):
            user.myq_username = form.data['myq_username']
        if form.data.get('myq_password', None):
            user.myq_password = form.data['myq_password']
        if form.data.get('name', None):
            first_name, last_name = self._split_name_into_first_and_last(form.data['name'])
            user.first_name = first_name
            user.last_name = last_name
        if form.data.get('email', None):
            user.email = form.data['email']
        user.save()

    @staticmethod
    def _split_name_into_first_and_last(name):
        """ Splits the name into first and last names """
        parts = name.split()
        return parts[0][:30], ' '.join(parts[1:])[:150]
