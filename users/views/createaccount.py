""" View for creating an account """
from django.contrib.auth import login
from django.urls import reverse_lazy
from django.views.generic.edit import FormView

from ..forms import CreateAccountForm
from ..models import User

class CreateAccount(FormView):
    """ Create an account using the CreateAccountForm """
    template_name = 'registration/create_account.html'
    form_class = CreateAccountForm
    success_url = reverse_lazy('users:update_profile')

    def form_valid(self, form):
        """ Evaluates a valid form and creates an account """
        user = User.objects.create_user(form.data['username'], password=form.data['password'])
        login(self.request, user)
        return super().form_valid(form)
