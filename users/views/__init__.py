""" Views for the user management app """

__all__ = ['AuthenticationForm', 'CreateAccount', 'dummy_view', 'UpdateProfile', 'PasswordReset']

from django.contrib.auth.forms import AuthenticationForm
from django.http import HttpResponse
from .createaccount import CreateAccount
from .updateprofile import UpdateProfile
from .passwordreset import PasswordReset

def dummy_view(request, *args, **kwargs): #pylint: disable=unused-argument
    """ A dummy view for things that are not yet implemented """
    text = 'Not implemented yet; args are %s; kwargs are %s' % (args, kwargs) # pragma: nocover
    return HttpResponse(text.encode('utf-8')) # pragma: nocover
