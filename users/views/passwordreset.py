""" View for resetting password """
import logging

from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.urls import reverse_lazy
from django.views.generic.edit import FormView

from ..forms import PasswordResetForm

LOGGER = logging.getLogger(__name__)

@method_decorator(login_required, name='dispatch')
class PasswordReset(FormView):
    """ User interaction for resetting user password """
    template_name = 'registration/password_reset.html'
    form_class = PasswordResetForm
    success_url = reverse_lazy('users:update_profile')

    def post(self, request, *args, **kwargs):
        """ Handle a post request with a filled-out form """
        form = self.get_form()
        if not form.is_valid() or not request.user.check_password(form.data['old_password']):
            return self.form_invalid(form)
        # Form is valid and old password matches...
        request.user.set_password(form.data['new_password'])
        request.user.save()
        user = request.user
        # Log out, then log back in with the new password
        logout(request)
        login(request, user)
        return self.form_valid(form)
