""" Tests for the django forms used in the users app """
from uuid import uuid4
from django.test import TestCase

from ..forms import CreateAccountForm, UpdateProfileForm, PasswordResetForm
from ..models import User

class TestCreateAccountForm(TestCase):
    """ Tests the CreateAccountForm with various sets of data """

    def setUp(self):
        self.secure_password_1 = str(uuid4())
        self.secure_password_2 = str(uuid4())
        self.secure_password_3 = str(uuid4())
        self.weak_password_1 = 'password'
        self.weak_password_2 = 'password1'
        self.username_1 = str(uuid4()).replace('-', '_')
        self.username_with_spaces = 'this username has spaces uh oh'

    def test_no_username_fails(self):
        """ Test that a blank or no username in the field is not valid """
        data = dict(password=self.secure_password_1, confirm_password=self.secure_password_1)
        self.assertFalse(CreateAccountForm(data).is_valid())
        data['username'] = ''
        self.assertFalse(CreateAccountForm(data).is_valid())

    def test_no_password_fails(self):
        """ Test that no password fails to create an account """
        data = dict(username=self.username_1, confirm_password=self.secure_password_1)
        self.assertFalse(CreateAccountForm(data).is_valid())

    def test_no_confirm_password_fails(self):
        """ Test that no confirm password fails to create an account """
        data = dict(username=self.username_1, password=self.secure_password_1)
        self.assertFalse(CreateAccountForm(data).is_valid())

    def test_different_confirm_password_fails(self):
        """ Test that password/confirm passwords that don't match are not valid """
        data = dict(username=self.username_1, password=self.secure_password_1,
                    confirm_password=self.secure_password_2)
        self.assertFalse(CreateAccountForm(data).is_valid())

    def test_valid_data(self):
        """ Test that valid use of the create account form is recognized as such """
        data = dict(username=self.username_1, password=self.weak_password_1,
                    confirm_password=self.weak_password_1)
        self.assertFalse(CreateAccountForm(data).is_valid())

    def test_username_with_whitespace_invalid(self):
        """ Test that whitespace in the username is invalid """
        data = dict(username=self.username_with_spaces, password=self.secure_password_1,
                    confirm_password=self.secure_password_1)
        self.assertFalse(CreateAccountForm(data).is_valid())

    def test_username_already_exists_fails(self):
        """ Test that form fails validation if the requested username already exists """
        data = dict(username=self.username_1, password=self.secure_password_1,
                    confirm_password=self.secure_password_1)
        self.assertTrue(CreateAccountForm(data).is_valid())
        User.objects.create_user(self.username_1, password=self.secure_password_1)
        self.assertFalse(CreateAccountForm(data).is_valid())

class TestUpdateProfileForm(TestCase):

    def setUp(self):
        # We need to create a user
        self.user = User.objects.create_user('testuser', email='testuser@email.com',
                                             password=str(uuid4()))

    def test_update_name(self):
        """ Tests that only updating the name in a profile works """
        data = dict(name='FirstName LastName')
        self.assertTrue(UpdateProfileForm(data).is_valid())

    def test_update_myq_username(self):
        """ Tests that only updating MyQ username in a profile works """
        data = dict(myq_username='iusemyq@garage.com')
        self.assertTrue(UpdateProfileForm(data).is_valid())

    def test_update_myq_password(self):
        """ Tests that only updating MyQ password in a profile works """
        data = dict(myq_password=str(uuid4()))
        self.assertTrue(UpdateProfileForm(data).is_valid())

    def test_update_email(self):
        """ Tests that only updating email in a profile works """
        data = dict(email='something@email.com')
        self.assertTrue(UpdateProfileForm(data).is_valid())

    def test_update_all_fields(self):
        data = dict(myq_username='iusemyq@garage.com', myq_password='thisisapassword',
                    name='FirstName MiddleName LastName', email='iusemyq@garage.com')
        self.assertTrue(UpdateProfileForm(data).is_valid())

class TestPasswordResetForm(TestCase):

    def setUp(self):
        self.old_password = str(uuid4())
        self.new_password = str(uuid4())
        self.user = User.objects.create_user('testuser', email='testuser@email.com',
                                             password=self.old_password)

    def test_update_password_old_password_needed(self):
        """ Test that update_password form requires an old password """
        data = dict(new_password=self.new_password, confirm_new_password=self.new_password)
        self.assertFalse(PasswordResetForm(data).is_valid())

    def test_update_password_new_password_needed(self):
        """ Test that the update_password form requires a new password """
        data = dict(old_password=self.old_password, confirm_new_password=self.new_password)
        self.assertFalse(PasswordResetForm(data).is_valid())

    def test_update_password_confirm_new_password_needed(self):
        """ Test that the update_password form requires a new password confirmation """
        data = dict(old_password=self.old_password, new_password=self.new_password)
        self.assertFalse(PasswordResetForm(data).is_valid())

    def test_update_password_valid_with_all_fields(self):
        """ Test that the update_password form requires a new password confirmation """
        data = dict(old_password=self.old_password, new_password=self.new_password,
                    confirm_new_password=self.new_password)
        self.assertTrue(PasswordResetForm(data).is_valid())

    def test_update_fails_with_confirm_password_not_matching(self):
        """ Test that confirm_password not matching fails the form validation """
        data = dict(old_password=self.old_password, new_password=self.new_password,
                    confirm_new_password=str(uuid4()))
        self.assertFalse(PasswordResetForm(data).is_valid())
