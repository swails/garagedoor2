from django.test import TestCase

from ..models import User

class TestUserModel(TestCase):

    def test_create_new_user(self):
        """ Tests the creation of a new user """
        user = User.objects.create_user(username='admin', password='password', first_name='John',
                                        last_name='Smith')
        user.save()

        user2 = User.objects.get(username='admin')
        self.assertEqual(user2.username, 'admin')
        self.assertTrue(user2.check_password('password'))
        self.assertEqual(user2.first_name, 'John')
        self.assertEqual(user2.last_name, 'Smith')

    def test_user_display_name(self):
        """ Tests the proper behavior of User.display_name """
        user = User.objects.create_user(username='user1', password='password', first_name='John',
                                        last_name='Smith')

        self.assertEqual(user.display_name(), 'John Smith')

        user = User.objects.create_user(username='user2', password='password')

        self.assertEqual(user.display_name(), 'user2')
