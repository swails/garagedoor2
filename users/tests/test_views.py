""" Tests the various views implemented for dealing with user accounts """
from unittest.mock import Mock

from django.contrib.auth.models import AnonymousUser
from django.contrib.sessions.models import Session
from django.test import RequestFactory, TestCase

from garagedoor2.tests import TestCaseWithUser
from users.models import User

from ..views import UpdateProfile, CreateAccount, PasswordReset

class UserSetupMixin(object):

    factory = RequestFactory()

    def setUp(self):
        self.user = self.create_user('me@email.com', 'thisissupersecure1!', 'John Q.', 'Tester',
                                     myq_username='me@garage.com', myq_password='some-password')

class TestUpdateProfile(UserSetupMixin, TestCaseWithUser):
    """ Tests the UpdateProfile view class """

    def test_update_fails_without_login(self):
        """ Test that attempt to update user account without logging in fails """
        data = {'myq_username': 'you@garage.com', 'myq_password': 'password'}
        request = self.factory.post('/users/profile', data=data)
        request.user = AnonymousUser()

        UpdateProfile.as_view()(request)

        self.user.refresh_from_db()

        self.assertEqual(self.user.username, 'me@email.com')
        self.assertTrue(self.user.check_password('thisissupersecure1!'))
        self.assertEqual(self.user.first_name, 'John Q.')
        self.assertEqual(self.user.last_name, 'Tester')
        self.assertEqual(self.user.myq_username, 'me@garage.com')
        self.assertEqual(self.user.myq_password, 'some-password')

    def test_update_succeeds_with_login(self):
        """ Test that attempt to update user account when logged in passes """
        data = {'myq_username': 'you@garage.com', 'myq_password': 'password'}
        request = self.factory.post('/users/profile', data=data)
        request.user = self.user

        UpdateProfile.as_view()(request)

        self.user.refresh_from_db()

        self.assertEqual(self.user.username, 'me@email.com')
        self.assertTrue(self.user.check_password('thisissupersecure1!'))
        self.assertEqual(self.user.first_name, 'John Q.')
        self.assertEqual(self.user.last_name, 'Tester')
        self.assertEqual(self.user.myq_username, 'you@garage.com')
        self.assertEqual(self.user.myq_password, 'password')

    def test_update_name(self):
        """ Test that attempt to update name is properly processed """
        data = {'name': 'My First And Middle Names'}
        request = self.factory.post('/users/profile', data=data)
        request.user = self.user

        UpdateProfile.as_view()(request)

        self.user.refresh_from_db()

        self.assertEqual(self.user.username, 'me@email.com')
        self.assertTrue(self.user.check_password('thisissupersecure1!'))
        self.assertEqual(self.user.first_name, 'My')
        self.assertEqual(self.user.last_name, 'First And Middle Names')
        self.assertEqual(self.user.myq_username, 'me@garage.com')
        self.assertEqual(self.user.myq_password, 'some-password')

    def test_update_email(self):
        """ Test that attempt to update email is properly processed """
        data = {'email': 'me@email2.com'}
        request = self.factory.post('/users/profile', data=data)
        request.user = self.user

        UpdateProfile.as_view()(request)

        self.user.refresh_from_db()

        self.assertEqual(self.user.username, 'me@email.com')
        self.assertEqual(self.user.email, 'me@email2.com')
        self.assertTrue(self.user.check_password('thisissupersecure1!'))
        self.assertEqual(self.user.first_name, 'John Q.')
        self.assertEqual(self.user.last_name, 'Tester')
        self.assertEqual(self.user.myq_username, 'me@garage.com')
        self.assertEqual(self.user.myq_password, 'some-password')

class TestPasswordReset(UserSetupMixin, TestCaseWithUser):
    """ Tests the PasswordReset view class """
    new_password = 'somenewstrongpassword'

    def setUp(self):
        super().setUp()
        from users.views import passwordreset
        passwordreset.login = self.login = Mock()
        passwordreset.logout = self.logout = Mock()

    def tearDown(self):
        from django.contrib.auth import login, logout
        from users.views import passwordreset
        passwordreset.login = login
        passwordreset.logout = logout

    def test_password_reset_not_logged_in_fails(self):
        """ Tests that you must be logged in to do a password reset """
        data = {'old_password': 'thisissupersecure1!', 'new_password': self.new_password,
                'confirm_new_password': self.new_password}

        request = self.factory.post('/users/profile/password-reset', data=data)
        request.user = AnonymousUser()

        PasswordReset.as_view()(request)

        self.user.refresh_from_db()

        self.assertTrue(self.user.check_password('thisissupersecure1!'))

    def test_password_reset_fails_if_confirm_fails(self):
        """ Tests that the password reset functionality fails if the confirmation fails """
        data = {'old_password': 'thisissupersecure1!', 'new_password': self.new_password,
                'confirm_new_password': 'thispassworddoesnotmatch'}

        request = self.factory.post('/users/profile/password-reset', data=data)
        request.user = self.user

        PasswordReset.as_view()(request)

        self.user.refresh_from_db()

        self.assertTrue(self.user.check_password('thisissupersecure1!'))

    def test_password_reset_succeeds(self):
        """ Test a successful use of password-reset """
        data = {'old_password': 'thisissupersecure1!', 'new_password': self.new_password,
                'confirm_new_password': self.new_password}

        request = self.factory.post('/users/profile/password-reset', data=data)
        request.user = self.user

        response = PasswordReset.as_view()(request)

        self.user.refresh_from_db()

        self.assertTrue(self.user.check_password(self.new_password))
        # Make sure we logged out and back in
        self.login.assert_called_once()
        self.logout.assert_called_once()

class TestCreateAccount(TestCase):
    """ Tests the account creation view """

    factory = RequestFactory()

    def setUp(self):
        from users.views import createaccount
        self.username = 'iamauser@email.com'
        self.password = 'thisisastrongpassword'
        # Make sure there are no users yet
        self.assertEqual(len(User.objects.all()), 0)
        createaccount.login = self.login = Mock()

    def tearDown(self):
        from users.views import createaccount
        from django.contrib.auth import login
        createaccount.login = login

    def test_fails_with_bad_confirmation(self):
        """ Tests that account creation fails with bad confirmation """
        data = {'username': self.username, 'password': self.password, 'confirm_password': 'nomatch'}

        request = self.factory.post('/users/signup', data=data)
        request.user = AnonymousUser()

        CreateAccount.as_view()(request)

        self.assertEqual(len(User.objects.all()), 0)

    def test_create_account_works(self):
        """ Test that the account creation view works """
        data = {'username': self.username, 'password': self.password,
                'confirm_password': self.password}

        request = self.factory.post('/users/signup', data=data)
        request.user = AnonymousUser()

        CreateAccount.as_view()(request)

        self.assertEqual(len(User.objects.all()), 1)
        user, = User.objects.all()
        self.assertTrue(user.check_password(self.password))
