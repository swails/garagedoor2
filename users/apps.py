""" Users sub-application in garagedoor Django application """
from django.apps import AppConfig

class UsersConfig(AppConfig):
    """ Configuration for users app """
    name = 'users'
