"""
Base user model, borrows heavily from Django's contributed auth app, but adds the additional
information associated with MyQ credentials
"""
import uuid

from django.db import models
from django.contrib.auth.models import AbstractUser

class User(AbstractUser):
    """ A user that has a MyQ account """
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    myq_username = models.CharField(max_length=256, null=True)
    myq_password = models.CharField(max_length=50, null=True)
    myq_security_token = models.CharField(max_length=1024, null=True)
    myq_account_id = models.CharField(max_length=1024, null=True)

    def display_name(self):
        """ Display name combines first and last names """
        if self.first_name or self.last_name:
            return ' '.join([self.first_name or '', self.last_name or '']).strip()
        return self.username
