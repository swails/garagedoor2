""" The URLs provided by the users app """
from django.contrib.auth.urls import urlpatterns as auth_urlpatterns
from django.urls import path
from django.views.generic import RedirectView

from . import views

app_name = 'users'

urlpatterns = auth_urlpatterns + [
    path('', RedirectView.as_view(pattern_name='users:login', permanent=False)),
    path('signup/', views.CreateAccount.as_view(), name='create_account'),
    path('profile/', views.UpdateProfile.as_view(), name='update_profile'),
    path('profile/password-reset', views.PasswordReset.as_view(), name='password_reset'),
]
