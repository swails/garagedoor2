""" Form to submit password resets """
from django import forms
from django.contrib.auth.password_validation import validate_password

class PasswordResetForm(forms.Form):
    """ Password reset form

    Parameters
    ----------
    old_password : str
        The old password (must validate against currently-set password)
    new_password : str
        The new password to use
    confirm_new_password : str
        Must match the new password above in order to pass validation
    """
    old_password = forms.CharField(max_length=128, widget=forms.PasswordInput(), required=True)
    new_password = forms.CharField(max_length=128, widget=forms.PasswordInput(),
                                   validators=[validate_password], required=True)
    confirm_new_password = forms.CharField(max_length=128, widget=forms.PasswordInput(),
                                           required=True)

    def clean(self):
        """ Ensures that new_pasword and confirm_new_password are identical """
        cleaned_data = self.cleaned_data
        if 'new_password' not in cleaned_data or 'confirm_new_password' not in cleaned_data:
            raise forms.ValidationError('Must specify a new password and confirm it')
        if cleaned_data['new_password'] != cleaned_data['confirm_new_password']:
            raise forms.ValidationError('Password confirmation failed: they are different')
        return super().clean()
