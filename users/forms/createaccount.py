""" Form for creating a new account """
from django.contrib.auth.password_validation import validate_password
from django.contrib.auth.validators import ASCIIUsernameValidator
from django import forms
from ..models import User

class CreateAccountForm(forms.Form):
    """ Create a new account

    Parameters
    ----------
    username : str
        The username to use
    password : str
        The password to use for authentication
    confirm_password : str
        Must be the same as password to pass form validation
    """
    username = forms.CharField(max_length=128, validators=[ASCIIUsernameValidator()])
    password = forms.CharField(max_length=128, widget=forms.PasswordInput,
                               validators=[validate_password])
    confirm_password = forms.CharField(max_length=128, widget=forms.PasswordInput)

    def clean_username(self):
        """ Ensures that the username is unique """
        username = self.cleaned_data['username']
        if User.objects.filter(username=username):
            raise forms.ValidationError('Username %s already exists' % username)
        return username

    def clean(self):
        """ Ensures that password and confirm_password match """
        cleaned_data = super().clean()
        password = cleaned_data.get('password', None)
        confirm_password = cleaned_data.get('confirm_password', None)
        if password is None:
            raise forms.ValidationError('Must specify a password')
        if password != confirm_password:
            raise forms.ValidationError('Password confirmation does not match')
