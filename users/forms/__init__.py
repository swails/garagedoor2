""" Collection of all forms in this app """
__all__ = ['CreateAccountForm', 'PasswordResetForm', 'UpdateProfileForm']

from .createaccount import CreateAccountForm
from .passwordreset import PasswordResetForm
from .updateprofile import UpdateProfileForm
