""" Form for updating user profile """
from django import forms

class UpdateProfileForm(forms.Form):
    """ Form with options for updating profile

    Parameters
    ----------
    myq_username : str
        The username used in MyQ
    myq_password : str
        The password used in MyQ
    name : str
        The full name for the user
    email : str
        The email address to use for this user
    """
    myq_username = forms.CharField(max_length=128, required=False, label='MyQ Username')
    myq_password = forms.CharField(max_length=128, widget=forms.PasswordInput(render_value=True),
                                   required=False, label='MyQ Password')
    name = forms.CharField(max_length=1024, required=False)
    email = forms.EmailField(required=False)
