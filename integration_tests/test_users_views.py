""" Tests the Garage Door App Views for the Users app """
#pylint: disable=line-too-long
from users import models

from . import ViewClassStaticLiveServerTestCase, TestViewRequiresLogin

class TestCreateEditAccountView(ViewClassStaticLiveServerTestCase):
    """ Test create/edit account views """

    @classmethod
    def setUpClass(cls):
        """ Set up once for every test """
        super().setUpClass()
        cls.myq_password = 'NotValidated'

    def setUp(self):
        """ Point the website to the signup page """
        self.webdriver.get('%s%s' % (self.live_server_url, '/users/signup'))

    def test_create_account_form(self):
        """ Tests that the create_account form works properly """
        self._set_input('username', self.username)
        self._set_input('password', self.secure_password)
        self._set_input('confirm_password', self.secure_password)
        self.webdriver.find_element_by_name('create_account').click()
        # Ensure the account was created
        self.assertTrue(bool(models.User.objects.filter(username=self.username)))

    def test_create_account_form_invalid_data(self):
        """ Tests that the create_account form properly catches errors """
        self._set_input('username', self.username)
        self._set_input('password', 'weak')
        self._set_input('confirm_password', 'weak')
        self.webdriver.find_element_by_name('create_account').click()
        # Ensure the account was not created
        self.assertFalse(bool(models.User.objects.filter(username=self.username).all()))

class TestUpdateProfileView(TestViewRequiresLogin):
    """ Tests update profile view for logged in user """

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.myq_username = 'user1@email.com'
        cls.myq_password = 'myqpasswordcanbeanything'
        cls.email = 'iusemyq@garage.com'
        cls.simple_name = 'FirstName LastName'.split()
        cls.three_names = 'FirstName MiddleName LastName'.split()
        cls.long_first_name = 'ThisIsAVeryLongFirstNameThatMustBeAtLeast30CharactersInOrderToStressTheDatabase Smith'.split()
        cls.long_last_name = ('Jane ThisIsAVeryLongLastNameThatMustBe At Least 150 Characters '
                              'InOrderToStress The Database But it need not All Be in a single '
                              'NameSince only the FirstWord Is Classified As The First Name').split()

    def setUp(self):
        super().setUp()
        self.assertGreater(len(self.long_first_name[0]), 30)
        self.assertGreater(len(' '.join(self.long_last_name[1:])), 150)
        # Now fetch the update-profile page
        self.webdriver.get('%s%s' % (self.live_server_url, '/users/profile'))
        self._set_input('myq_password', self.myq_password)
        self._set_input('myq_username', self.myq_username)
        self._set_input('email', self.email)

    def test_basic_profile_update(self):
        """ Tests the profile updating functionality when the name is simple """
        self._set_input('name', ' '.join(self.simple_name))
        self.webdriver.find_element_by_name('update_profile').click()
        user = models.User.objects.get(username=self.username)
        self._check_all_common_metadata(user)
        self.assertEqual(user.first_name, self.simple_name[0])
        self.assertEqual(user.last_name, self.simple_name[1])

    def test_3name_profile_update(self):
        """ Tests that name is split into first name and everything else in the last name """
        self._set_input('name', ' '.join(self.three_names))
        self.webdriver.find_element_by_name('update_profile').click()
        user = models.User.objects.get(username=self.username)
        self._check_all_common_metadata(user)
        self.assertEqual(user.first_name, self.three_names[0])
        self.assertEqual(user.last_name, ' '.join(self.three_names[1:]))

    def test_long_first_name_profile_update(self):
        """ Tests proper truncation of first name if it is too long """
        self._set_input('name', ' '.join(self.long_first_name))
        self.webdriver.find_element_by_name('update_profile').click()
        user = models.User.objects.get(username=self.username)
        self._check_all_common_metadata(user)
        self.assertEqual(user.first_name, self.long_first_name[0][:30])
        self.assertEqual(user.last_name, ' '.join(self.long_first_name[1:]))

    def test_long_last_name_profile_update(self):
        """ Tests proper truncation of last name if it is too long """
        self._set_input('name', ' '.join(self.long_last_name))
        self.webdriver.find_element_by_name('update_profile').click()
        user = models.User.objects.get(username=self.username)
        self._check_all_common_metadata(user)
        self.assertEqual(user.first_name, self.long_last_name[0])
        self.assertEqual(user.last_name, ' '.join(self.long_last_name[1:])[:150])

    def _check_all_common_metadata(self, user):
        """ Checks all of the metadata common bewteen test class and user """
        self.assertEqual(user.myq_username, self.myq_username)
        self.assertEqual(user.myq_password, self.myq_password)
        self.assertEqual(user.email, self.email)

class TestChangePassword(TestViewRequiresLogin):
    """ Tests interaction where we try to change the password """

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.new_secure_password = 'thisisastrongpassw0rdt00'

    def setUp(self):
        super().setUp()
        self.webdriver.get('%s%s' % (self.live_server_url, '/users/profile/password-reset'))

    def test_change_password_old_password_wrong(self):
        """ Tests that the old password must be right to change it """
        self._set_input('old_password', 'thisisnotright')
        self._set_input('new_password', self.new_secure_password)
        self._set_input('confirm_new_password', self.new_secure_password)
        self.webdriver.find_element_by_name('reset_password').click()
        user = models.User.objects.get(username=self.username)
        self.assertFalse(user.check_password(self.new_secure_password))
        self.assertTrue(user.check_password(self.secure_password))

    def test_change_password_confirm_new_password_wrong(self):
        """ Test that the new password must be confirmed to change it """
        self._set_input('old_password', self.secure_password)
        self._set_input('new_password', self.new_secure_password)
        self._set_input('confirm_new_password', 'thisissecurebutnotthesamepassword')
        self.webdriver.find_element_by_name('reset_password').click()
        user = models.User.objects.get(username=self.username)
        self.assertFalse(user.check_password(self.new_secure_password))
        self.assertTrue(user.check_password(self.secure_password))

    def test_change_password_new_password_must_be_strong(self):
        """ Test that the new password is tested for strength """
        self._set_input('old_password', self.secure_password)
        self._set_input('new_password', 'weak')
        self._set_input('confirm_new_password', 'weak')
        self.webdriver.find_element_by_name('reset_password').click()
        user = models.User.objects.get(username=self.username)
        self.assertFalse(user.check_password(self.new_secure_password))
        self.assertTrue(user.check_password(self.secure_password))

    def test_change_password_succeeds(self):
        """ Tests the change password functionality and ensure it works """
        self._set_input('old_password', self.secure_password)
        self._set_input('new_password', self.new_secure_password)
        self._set_input('confirm_new_password', self.new_secure_password)
        self.webdriver.find_element_by_name('reset_password').click()
        user = models.User.objects.get(username=self.username)
        self.assertTrue(user.check_password(self.new_secure_password))
        self.assertFalse(user.check_password(self.secure_password))
