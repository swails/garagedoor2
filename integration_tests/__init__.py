""" Integration test base class for launching a window with a selenium web driver """
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.support.ui import Select
from users.models import User

class ViewClassStaticLiveServerTestCase(StaticLiveServerTestCase):
    """ Gives a static live server test with a Selenium web driver """

    DEFAULT_USERNAME = 'testuser'
    DEFAULT_PASSWORD = 'Th1s1s@Str0ngP@ssw0rd'

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.webdriver = WebDriver()
        cls.webdriver.implicitly_wait(10)
        cls.username = cls.DEFAULT_USERNAME
        cls.secure_password = cls.DEFAULT_PASSWORD

    @classmethod
    def tearDownClass(cls):
        cls.webdriver.quit()
        super().tearDownClass()

    def _set_input(self, field, value):
        field_input = self.webdriver.find_element_by_name(field)
        field_input.send_keys(value)

    def _set_input_choice_text(self, field, value):
        field_input = Select(self.webdriver.find_element_by_name(field))
        field_input.select_by_visible_text(value)

class TestViewRequiresLogin(ViewClassStaticLiveServerTestCase):
    """ Base class for view testing that requires login """

    def setUp(self):
        # Log in
        self.webdriver.get('%s%s' % (self.live_server_url, '/users/signup'))
        self._set_input('username', self.username)
        self._set_input('password', self.secure_password)
        self._set_input('confirm_password', self.secure_password)
        self.webdriver.find_element_by_name('create_account').click()
        self.user = User.objects.get(username=self.username)
