""" Tests the Garage Door App Views for the garagedoor App"""
#pylint: disable=too-many-ancestors
import random
import uuid
from datetime import date, time
from string import ascii_letters
from unittest import mock

from django.contrib.auth.models import AnonymousUser
from django.http import Http404, HttpRequest
from selenium.common.exceptions import NoSuchElementException

import users.models
from garagedoor import models, views
from garagedoor.views import updatecode

from . import TestViewRequiresLogin, ViewClassStaticLiveServerTestCase


class CodeCreatorMixin:
    """ Mixin to create door codes """

    def _create_door_code(self, name, time_zone='America/New_York', go_back_to_codes=True):
        """ Create a door code with the given information """
        self.webdriver.find_element_by_name('createcode').click()
        self._set_input('name', name)
        self._set_input('time_zone', time_zone)
        self.webdriver.find_element_by_name('create_code').click()
        self.code = models.DoorCode.objects.get(name=name)
        if go_back_to_codes:
            self.webdriver.find_element_by_name('back_to_all_codes').click()

class TestAllCodesView(TestViewRequiresLogin, CodeCreatorMixin):
    """ Tests the AllDoorCodesView view. The following tasks can be accomplished in this view:

    1. Create a new door code
    2. Select a door code to edit
    3. Delete a door code
    4. Clear a door code that is currently set

    This view should only be visible when logged in
    """
    def setUp(self):
        """ Make sure we go to /doorcontrol """
        super().setUp()
        self.webdriver.get('%s%s' % (self.live_server_url, '/doorcontrol'))

    def test_create_first_door_code(self):
        """ Tests creating an initial door code """
        self.assertEqual(len(models.DoorCode.objects.all()), 0)
        self._create_door_code('John Smith', 'America/New_York')
        # Now make sure that the new code is present and belongs to the logged-in user
        self.assertEqual(len(models.DoorCode.objects.all()), 1)
        code = models.DoorCode.objects.get(name='John Smith')
        self.assertIs(code.passcode, None)
        self.assertEqual(code.timezone, 'America/New_York')
        self.assertEqual(code.user, self.user)

    def test_view_door_code_usage_history(self):
        """ Tests that the door code usage history page is accessible """
        self._create_door_code('John Smith', 'America/New_York')
        # Now go to the history of that code
        self.webdriver.find_element_by_name('history1').click()

    def test_delete_door_code(self):
        """ Tests deleting a door code """
        self._create_door_code('John Smith', 'America/New_York')
        self.assertEqual(len(models.DoorCode.objects.all()), 1)
        self.webdriver.find_element_by_name('delete1').click()
        self.assertEqual(len(models.DoorCode.objects.all()), 0)

    def test_delete_one_door_code(self):
        """ Tests deleting only one door code if multiple are set """
        self._create_door_code('Code 1')
        self._create_door_code('Code 2')
        self.assertEqual(len(models.DoorCode.objects.all()), 2)
        self.webdriver.find_element_by_name('delete2').click()
        self.assertEqual(len(models.DoorCode.objects.all()), 1)
        code = models.DoorCode.objects.get(name='Code 1')
        self.assertIsInstance(code, models.DoorCode)

    def test_clear_door_code(self):
        """ Tests clearing a door code """
        self._create_door_code('Code 1')
        code = models.DoorCode.objects.get(name='Code 1')
        self.assertIs(code.passcode, None)
        code.set_passcode('1234')
        code.save()
        code.refresh_from_db()
        self.assertIsNot(code.passcode, None)
        self.webdriver.refresh()
        self.webdriver.find_element_by_name('clear1').click()
        code.refresh_from_db()
        self.assertIs(code.passcode, None)

    @staticmethod
    def _make_request(user, method='POST'):
        """ Creates a request object """
        request = HttpRequest()
        request.method = method
        assert request.method in ('POST', 'GET')
        request.user = user
        return request

    def test_clear_door_code_requires_post(self):
        """ Tests that clearing door code view requires POST """
        self._create_door_code('Code 1')
        code = models.DoorCode.objects.get(name='Code 1')
        code.set_passcode('1234')
        code.save()
        code.refresh_from_db()
        self.assertIsNot(code.passcode, None)
        request = self._make_request(self.user, 'GET')
        with self.assertRaises(Http404):
            views.clear_code(request, self.code.id)

    def test_clear_door_code_disallowed_for_non_owner(self):
        """ Tests that clearing a door code can only be done by the code owner """
        self._create_door_code('Code 1')
        code = models.DoorCode.objects.get(name='Code 1')
        code.set_passcode('1234')
        code.save()
        code.refresh_from_db()
        new_user = users.models.User.objects.create_user(username='somebody', password='password')
        request = self._make_request(new_user, 'POST')
        with self.assertRaises(Http404):
            views.clear_code(request, self.code.id)
        request = self._make_request(AnonymousUser, 'POST')
        with self.assertRaises(Http404):
            views.clear_code(request, self.code.id)

    def test_clearing_impossible_before_code_set(self):
        """ Test that there is no "clear" button before a passcode is set """
        self._create_door_code('Code 1')
        with self.assertRaises(NoSuchElementException):
            self.webdriver.find_element_by_name('clear1')

class TestOwnerUpdateCode(TestViewRequiresLogin, CodeCreatorMixin):
    """ Test cases for a user editing codes to their system """

    def setUp(self):
        """ Sets up webdriver to edit the code """
        super().setUp()
        self.webdriver.get('%s%s' % (self.live_server_url, '/doorcontrol'))
        # Create a code and navigate to the page where we can edit that page
        self._create_door_code('Code 1')
        self.webdriver.find_element_by_name('edit1').click()
        self.current_year = date.today().year

    def test_create_active_date_with_all_elements(self):
        """ Tests creating a new DoorCodeActiveDate with all options specified """
        num_active = len(models.DoorCodeActiveDate.objects.all())
        self._set_input_choice_text('start_date_month', 'January')
        self._set_input_choice_text('start_date_day', '1')
        self._set_input_choice_text('start_date_year', str(self.current_year))

        self._set_input_choice_text('end_date_month', 'January')
        self._set_input_choice_text('end_date_day', '1')
        self._set_input_choice_text('end_date_year', str(self.current_year + 8))

        self._set_input('start_time', '9:00')
        self._set_input('end_time', '16:00')

        self.webdriver.find_element_by_name('create_time').click()

        # Now make sure the code was created
        self.assertEqual(len(models.DoorCodeActiveDate.objects.all()), num_active + 1)
        self.assertEqual(len(models.DoorCodeActiveDate.objects.filter(code=self.code)), 1)
        code_active = models.DoorCodeActiveDate.objects.get(code=self.code)
        self.assertEqual(code_active.start_date, date(self.current_year, 1, 1))
        self.assertEqual(code_active.end_date, date(self.current_year + 8, 1, 1))
        self.assertEqual(code_active.start_time, time(9, 0, 0))
        self.assertEqual(code_active.end_time, time(16, 0, 0))

    def test_create_active_date_without_end_date(self):
        """ Tests creating a new DoorCodeActiveDate with all options but end_date specified """
        num_active = len(models.DoorCodeActiveDate.objects.all())
        self._set_input_choice_text('start_date_month', 'January')
        self._set_input_choice_text('start_date_day', '2')
        self._set_input_choice_text('start_date_year', str(self.current_year))

        self._set_input('start_time', '8:00')
        self._set_input('end_time', '17:00')

        self.webdriver.find_element_by_name('create_time').click()

        # Now make sure the code was created
        self.assertEqual(len(models.DoorCodeActiveDate.objects.all()), num_active + 1)
        self.assertEqual(len(models.DoorCodeActiveDate.objects.filter(code=self.code)), 1)
        code_active = models.DoorCodeActiveDate.objects.get(code=self.code)
        self.assertEqual(code_active.start_date, date(self.current_year, 1, 2))
        self.assertEqual(code_active.start_time, time(8, 0, 0))
        self.assertEqual(code_active.end_time, time(17, 0, 0))

class TestAnonymousPasscodeSetter(ViewClassStaticLiveServerTestCase, CodeCreatorMixin):
    """ Tests passcode setter that is anonymous """

    def setUp(self):
        """ Creates the code and orients to set the passcode """
        super().setUp()
        self.owner = users.models.User.objects.create_user(
            username='codeowner', password='codepassword'
        )
        self.code = models.DoorCode(name='Test Code', timezone='America/New_York', user=self.owner)
        self.code.save()
        self.webdriver.get('%s%s' % (self.live_server_url, '/doorcontrol/%s' % self.code.id))

    def _set_passcode(self, passcode):
        """ Sets a passcode on a code """
        self._set_input('passcode', passcode)
        self._set_input('confirm_passcode', passcode)
        self.webdriver.find_element_by_name('create_code').click()
        self.code.refresh_from_db()


class TestAnonymousUpdateCode(TestAnonymousPasscodeSetter):
    """ Tests anonymous updating of a code """

    def test_set_passcode(self):
        """ Tests an anonymous user setting a passcode """
        self.assertIs(self.code.passcode, None)
        self._set_passcode('1234')
        self.assertIsNot(self.code.passcode, None)
        self.assertTrue(self.code.check_passcode('1234'))
        self.assertFalse(self.code.check_passcode('foobar'))

    def test_confirm_passcode(self):
        """ Tests an anonymous submitting a passcode """
        self.assertEqual(len(models.DoorCodeUsed.objects.filter(code=self.code)), 0)
        self._set_passcode('1234')
        self._set_input('passcode', '1234')
        self.webdriver.find_element_by_name('submit_code').click()
        # There should be one DoorCodeUsage now
        self.assertEqual(len(models.DoorCodeUsed.objects.filter(code=self.code)), 1)
        self.assertFalse(models.DoorCodeUsed.objects.get(code=self.code).valid)
        # There are no valid times, so this is not active
        self.assertEqual(models.DoorCodeUsed.objects.get(code=self.code).reason, 'Not Active')

class TestNonOwnerUpdateCode(TestViewRequiresLogin, TestAnonymousUpdateCode):
    """ Tests someone that is logged in, but is not the owner of a code """

    def setUp(self):
        """ Calls all of the necessary setUp routines """
        TestViewRequiresLogin.setUp(self)
        TestAnonymousUpdateCode.setUp(self)

class TestDateCodeActiveManipulation(TestViewRequiresLogin, CodeCreatorMixin):
    """ Tests date code under active manipulation """

    def setUp(self):
        """ Create a code and create a non-owner """
        super().setUp()
        self.webdriver.get('%s%s' % (self.live_server_url, '/doorcontrol'))
        self._create_door_code('Code 1', go_back_to_codes=False)
        self.webdriver.find_element_by_name('create_time').click()
        self.assertEqual(len(models.DoorCodeActiveDate.objects.filter(code=self.code)), 1)
        self.active_time = models.DoorCodeActiveDate.objects.get(code=self.code)
        self.not_owner = users.models.User.objects.create_user(username='not_the_owner',
                                                               password='thisisastrongpassword')

    @staticmethod
    def _make_request(user=AnonymousUser, method='POST'):
        """ Makes a request to send to the test """
        request = HttpRequest()
        request.user = user
        request.method = method
        assert method in ('POST', 'GET')
        return request

    def test_delete_active_time(self):
        """ Tests the delete active time button works properly """
        self.webdriver.find_element_by_name('delete1').click()
        self.assertEqual(len(models.DoorCodeActiveDate.objects.filter(code=self.code)), 0)

    def test_delete_active_time_not_authenticated(self):
        """ Tests that deleting an active time is rejected for an unauthenticated user """
        request = self._make_request()
        with self.assertRaises(Http404):
            views.delete_date_active_code(request, self.code.id, self.active_time.id)
        self.assertEqual(len(models.DoorCodeActiveDate.objects.filter(code=self.code)), 1)

    def test_delete_active_time_get(self):
        """ Tests that deleting an active time is rejected via a GET request """
        request = self._make_request(self.user, 'GET')
        with self.assertRaises(Http404):
            views.delete_date_active_code(request, self.code.id, self.active_time.id)
        self.assertEqual(len(models.DoorCodeActiveDate.objects.filter(code=self.code)), 1)

    def test_delete_active_time_bad_time_id(self):
        """ Tests that deleting an active time fails if the active time ID does not exist """
        request = self._make_request(self.user, 'POST')
        with self.assertRaises(Http404):
            views.delete_date_active_code(request, self.code.id, 10)

    def test_delete_active_time_id_belongs_to_another_code(self):
        """ Tests that deleting an active time fails if it belongs to another user's code """
        code = models.DoorCode(name='Code2', timezone='America/New_York', user=self.not_owner)
        code.save()
        unaligned_active_time = models.DoorCodeActiveDate(code=code, start_date=date.today())
        unaligned_active_time.save()
        request = self._make_request(self.user, method='POST')
        with self.assertRaises(Http404):
            views.delete_date_active_code(request, self.code.id, unaligned_active_time.id)

    def test_delete_active_time_id_belongs_to_another_code2(self):
        """ Tests that deleting an active time fails if it belongs to another code of the user """
        code = models.DoorCode(name='Code2', timezone='America/New_York', user=self.user)
        code.save()
        unaligned_active_time = models.DoorCodeActiveDate(code=code, start_date=date.today())
        unaligned_active_time.save()
        request = self._make_request(self.user, 'POST')
        with self.assertRaises(Http404):
            views.delete_date_active_code(request, self.code.id, unaligned_active_time.id)

    def test_delete_active_time_bad_code_id(self):
        """ Tests that deleting an active time when the code ID doesn't exist results in 404 """
        request = self._make_request(self.user, 'POST')
        with self.assertRaises(Http404):
            views.delete_date_active_code(request, str(uuid.uuid4()), self.active_time.id)

    def test_delete_active_time_bad_owner(self):
        """ Tests that trying to delete a code that you do not own fails """
        request = self._make_request(self.not_owner, 'POST')
        with self.assertRaises(Http404):
            views.delete_date_active_code(request, self.code.id, self.active_time.id)

    def test_delete_active_code_works(self):
        """ Tests that an authenticated user can delete an active time that exists correctly """
        request = self._make_request(self.user)
        views.delete_date_active_code(request, self.code.id, self.active_time.id)
        self.assertEqual(len(models.DoorCodeActiveDate.objects.filter(code=self.code)), 0)

    def test_delete_code_unauthenticated_fails(self):
        """ Tests that deleting a code fails for an unauthenticated user """
        request = self._make_request()
        with self.assertRaises(Http404):
            views.delete_code(request, self.code.id)

    def test_delete_code_get_fails(self):
        """ Tests that hitting the code delete end-point with a GET request returns a 404 """
        request = self._make_request(user=self.user, method='GET')
        with self.assertRaises(Http404):
            views.delete_code(request, self.code.id)

    def test_delete_code_fails_with_bad_code_id(self):
        """ Tests that deleting a code fails if the code ID does not exist """
        request = self._make_request(user=self.user, method='POST')
        with self.assertRaises(Http404):
            views.delete_code(request, str(uuid.uuid4()))

    def test_delete_code_fails_with_unaligned_user(self):
        """ Tests that deleting a code you do not own fails with a 404 """
        request = self._make_request(user=self.not_owner, method='POST')
        with self.assertRaises(Http404):
            views.delete_code(request, self.code.id)

    def test_delete_code_works_for_aligned_user(self):
        """ Tests that deleting a code works if the code exists and is owned by the user """
        request = self._make_request(user=self.user, method='POST')
        views.delete_code(request, self.code.id)
        self.assertEqual(len(models.DoorCode.objects.filter(user=self.user)), 0)

class TestSubmitCodeView(TestAnonymousPasscodeSetter):
    """ Tests submitting a passcode """

    def setUp(self):
        """ Mock toggle door so we don't actually try to hit MyQ servers """
        super().setUp()
        self.passcode = ''.join([random.choice(ascii_letters) for i in range(10)])
        self._set_passcode(self.passcode)
        # Mock the check_dates_and_toggle_door call
        updatecode.check_dates_and_toggle_door = mock.MagicMock(return_value='MOCK CALLED')

    @staticmethod
    def _make_request(passcode, method='POST'):
        """ Create a request object to send to the service """
        request = HttpRequest()
        request.method = method
        request.user = AnonymousUser
        assert request.method in ('POST', 'GET'), 'Invalid verb on request'
        request.POST = dict(passcode=passcode)
        return request

    def test_confirm_passcode_correct(self):
        """ Tests the correct usage of the confirm_passcode view """
        request = self._make_request(self.passcode)
        response = views.confirm_code(request, self.code.id)
        updatecode.check_dates_and_toggle_door.assert_called_with(self.code)
        self.assertTrue(b'MOCK CALLED' in response.content)

    def test_confirm_passcode_requires_post(self):
        """ Tests that confirm_passcode must be called with a POST request """
        request = self._make_request(self.passcode, 'GET')
        with self.assertRaises(Http404):
            views.confirm_code(request, self.code.id)

    def test_confirm_passcode_fails_with_incorrect_code(self):
        """ Tests that confirm_passcode fails with an incorrect code """
        request = self._make_request('this is not the correct passcode')
        response = views.confirm_code(request, self.code.id)
        updatecode.check_dates_and_toggle_door.assert_not_called()
        self.assertIn(b'Code is incorrect', response.content)
