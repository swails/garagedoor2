""" A view that shows all codes associated with a particular user """
import logging

import pytz
from django.contrib.auth.decorators import login_required
from django.http import Http404
from django.utils.decorators import method_decorator
from django.views import generic

from ..models import DoorCode, DoorCodeUsed

LOGGER = logging.getLogger(__name__)

@method_decorator(login_required, name='get')
class DoorCodeHistoryView(generic.ListView):
    """ Displays a view with all of the codes belonging to a specific user """
    template_name = 'garagedoor/code_history.html'
    context_object_name = 'usage_history'

    def get_queryset(self):
        """ Raises 404 if authenticated user is not the code owner """
        code = DoorCode.objects.get(id=self.kwargs['pk'])
        if code.user != self.request.user:
            LOGGER.warning("User tried accessing somebody else's code")
            raise Http404('Code not found')
        return DoorCodeUsed.objects.filter(code=code)

    def get_context_data(self, *, object_list=None, **kwargs):
        """ Add the code to the context data and the iterator """
        context = super().get_context_data(object_list=object_list, **kwargs)
        context['code'] = code = DoorCode.objects.get(id=self.kwargs['pk'])
        converted_times = [active.time.astimezone(pytz.timezone(code.timezone)).replace(tzinfo=None)
                           for active in self.get_queryset()]
        context['active_iterator'] = zip(self.get_queryset(), converted_times)
        return context
