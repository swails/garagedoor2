"""
This class is responsible for allowing users to update when a code is active and non-users to
set the code password and *use* it
"""
import logging
from datetime import date

import pytz
from django.http import Http404
from django.shortcuts import redirect, render
from django.urls import reverse_lazy, reverse
from django.utils import timezone
from django.views.generic import FormView

from ..forms import UserUpdateDoorCodeForm, SubmitPasscodeForm, CreateNewPasscodeForm
from ..models import DoorCode, DoorCodeActiveDate, DoorCodeUsed
from ..myq import MyQGarageDoor, APPID, SERVICE_URL, DoorState, MyQError

LOGGER = logging.getLogger(__name__)

class UpdateUseCodeView(FormView):
    """ The form we use depends on several things:

    1) Whether or not this code belongs to the currently logged-in user
    2) The code is initialized with a password by a non-logged-in-user
    """

    def __init__(self, *args, **kwargs):
        self._my_code = None
        super().__init__(*args, **kwargs)

    @property
    def my_code(self):
        """ The code that this view is built around """
        if self._my_code is None:
            LOGGER.debug('Getting keyword arguments for DoorCode view: %s', self.kwargs)
            self._my_code = DoorCode.objects.get(id=self.kwargs['pk'])
        return self._my_code

    def user_owns_code(self):
        """ Returns True if the authenticated user also owns this code """
        return self.request.user.is_authenticated and self.request.user == self.my_code.user

    def get_form_class(self):
        """ Gets the form class based on authentication status """
        if self.user_owns_code():
            return UserUpdateDoorCodeForm
        if self.my_code.passcode:
            return SubmitPasscodeForm
        return CreateNewPasscodeForm

    def get_initial(self):
        """ Initial data if the user is editing the code """
        if self.user_owns_code():
            return dict(start_date=date.today(), timezone='America/New_York')
        return dict()

    def get_template_names(self):
        """ Template depends on whether or not user owns the code and if the code was set """
        if self.user_owns_code():
            return ['garagedoor/user_edit_code.html']
        if self.my_code.passcode:
            return ['garagedoor/submit_code.html']
        return ['garagedoor/anonymous_create_code.html']

    def get_context_data(self, **kwargs):
        """ Adds data available to the template """
        context = super().get_context_data()
        context['code'] = self.my_code
        if self.request.user.is_authenticated and self.request.user == self.my_code.user:
            context['code_active_times'] = DoorCodeActiveDate.objects.filter(code=self.my_code)
        else:
            context['owner'] = self.my_code.user
        context.update(kwargs)
        return context

    def form_valid(self, form):
        """
        How to handle a valid form. If user owns the code, add an active date. If not, add a
        passcode if necessary. Otherwise, nothing is necessary
        """
        if self.user_owns_code():
            data = form.data
            start = date(int(form.data['start_date_year']),
                         int(form.data['start_date_month']),
                         int(form.data['start_date_day']))
            if form.data.get('end_date_year', None) not in (None, 0, '0'):
                try:
                    end = date(int(form.data['end_date_year']),
                               int(form.data['end_date_month']),
                               int(form.data['end_date_day']))
                except ValueError:
                    LOGGER.error('Failed converting %s, %s, %s', form.data['end_date_year'],
                                 form.data['end_date_month'], form.data['end_date_day'])
                    end = None
            else:
                end = None
            new_date = DoorCodeActiveDate(
                code=self.my_code,
                start_date=start,
                repeat=form.data.get('repeat', None) or None,
                end_date=end,
                start_time=form.data.get('start_time', None) or None,
                end_time=form.data.get('end_time', None) or None,
            )
            new_date.save()
        elif not self.my_code.passcode:
            data = form.data
            self.my_code.set_passcode(data['passcode'])
            self.my_code.save()
        return super().form_valid(form)

    def get_success_url(self):
        """ Redirect to code updating or inputting """
        context = self.get_context_data()
        return reverse_lazy('garagedoor:update_code', args=[context['code'].id])

def delete_date_active_code(request, code_pk, active_pk):
    """ Deletes a particular schedule of when a specific code is active """
    if request.method != 'POST' or not request.user.is_authenticated:
        LOGGER.warning('Tried to delete an active code either unauthenticated or not a POST')
        raise Http404('Page Not Found')
    try:
        code = DoorCode.objects.get(id=code_pk)
        active = DoorCodeActiveDate.objects.get(id=active_pk)
    except (DoorCode.DoesNotExist, DoorCodeActiveDate.DoesNotExist) as err:
        LOGGER.exception('Object not found')
        raise Http404('Page Not Found') from err
    if code.user != request.user or active.code != code:
        LOGGER.error('Authenticated user does not own code or code does not own active time')
        raise Http404('Page Not Found')
    active.delete()
    return redirect(reverse_lazy('garagedoor:update_code', args=[code.id]))

def delete_code(request, pk):
    """ Deletes a particular schedule of when a specific code is active """
    if request.method != 'POST' or not request.user.is_authenticated:
        LOGGER.warning('Tried to delete an active code either unauthenticated or not a POST')
        raise Http404('Page Not Found')
    try:
        code = DoorCode.objects.get(id=pk)
    except DoorCode.DoesNotExist as err:
        LOGGER.exception('Object not found')
        raise Http404('Page Not Found') from err
    if code.user != request.user:
        LOGGER.error('Authenticated user does not own code')
        raise Http404('Page Not Found')
    code.delete()
    return redirect(reverse_lazy('garagedoor:index'))

def confirm_code(request, pk):
    """ Confirms a code that was passed in """
    if request.method != 'POST':
        LOGGER.warning('Confirm code hit with %s request', request.method)
        raise Http404('Page Not Found')
    code = DoorCode.objects.get(id=pk)
    form = SubmitPasscodeForm(request.POST)
    context = dict(return_url=reverse('garagedoor:update_code', args=[code.id]))
    now = timezone.now().astimezone(pytz.timezone(code.timezone))
    code_used = DoorCodeUsed(code=code, time=now, valid=False)
    if code.check_passcode(form.data['passcode']):
        LOGGER.info('Correct code!')
        check_response = check_dates_and_toggle_door(code)
        context['response'] = check_response
        if check_response == CodeCheckReturnValues.OPENED_DOOR:
            code_used.valid = True
            code_used.state = 'Opened'
        elif check_response == CodeCheckReturnValues.CLOSED_DOOR:
            code_used.valid = True
            code_used.state = 'Closed'
        elif check_response == CodeCheckReturnValues.NOT_ACTIVE:
            code_used.reason = 'Not Active'
        else:
            code_used.reason = 'Internal Error'
    else:
        code_used.reason = 'Bad Code'
        LOGGER.warning('Incorrect passcode used for %s', code)
        context['response'] = 'Code is incorrect'
    code_used.save()
    return render(request, 'garagedoor/submit_response.html', context=context)

def clear_code(request, pk):
    """ Clears a code if the owner wants to. Non-owner will have to re-set code """
    if request.method != 'POST':
        LOGGER.warning('Clear code hit with %s request', request.method)
        raise Http404('Page Not Found')
    code = DoorCode.objects.get(id=pk)
    if request.user.is_authenticated and request.user == code.user:
        LOGGER.info('Clearing the code')
        code.passcode = None
        code.save()
        return redirect(reverse_lazy('garagedoor:index'))
    LOGGER.warning('User not authenticated or does not own code')
    raise Http404('Page Not Found')

class CodeCheckReturnValues:
    """ Effectively an Enum for connection status with the corresponding message """
    UNABLE_TO_CONNECT = 'Unable to connect to garage door'
    CLOSED_DOOR = 'Closed the garage door'
    OPENED_DOOR = 'Opened the garage door'
    NOT_ACTIVE = 'Code is not active right now'

def check_dates_and_toggle_door(code):
    """
    Checks the current time to see if this code is permitted to be used at this time, then either
    opens or closes the garage door
    """
    # Get the current time *in the timezone
    now = timezone.now().astimezone(pytz.timezone(code.timezone))
    myq = MyQGarageDoor(code.user.myq_username, code.user.myq_password, SERVICE_URL, APPID,
                        code.user.myq_security_token)
    for active_time in DoorCodeActiveDate.objects.filter(code=code):
        if _active_date_matches(active_time, now):
            try:
                door_state = myq.check_door_state()
            except MyQError:
                return CodeCheckReturnValues.UNABLE_TO_CONNECT
            if myq.security_token != code.user.myq_security_token:
                # Update it in the database
                code.user.myq_security_token = myq.security_token
                code.user.save()
            myq.toggle_door()
            if door_state in {DoorState.Closed, DoorState.Closing}:
                return CodeCheckReturnValues.OPENED_DOOR
            return CodeCheckReturnValues.CLOSED_DOOR
    return CodeCheckReturnValues.NOT_ACTIVE

def _active_date_matches(active_time, now):
    """ Returns True if "now" is valid within the active time and False otherwise """
    today = date(now.year, now.month, now.day)
    LOGGER.debug('Looking at active time: %r', active_time)
    LOGGER.debug('Time and date now are %s, %s', now.time(), today)
    if today < active_time.start_date:
        LOGGER.debug('Today is earlier than our starting time')
        return False
    if active_time.end_date and today > active_time.end_date:
        LOGGER.debug('Today is later than the last permissible day')
        return False
    if active_time.repeat:
        # Make sure it lands on a proper day if the repeat interval is set
        LOGGER.debug('Checking if today falls on an interval of repeat')
        if (today - active_time.start_date).days % active_time.repeat != 0:
            LOGGER.debug('Does not fall on a proper day interval')
            return False
    if not active_time.end_date and not active_time.repeat and today != active_time.start_date:
        return False
    # If we reached here, it's active today. Now check if it's within the permissible time window
    if active_time.start_time and active_time.end_time:
        LOGGER.info('Checking if the code was used in the permissible time window')
        return active_time.start_time <= now.time() <= active_time.end_time
    # If we reach here, *only* start date can be set, so make sure that's today
    LOGGER.debug('Code is active any time today')
    return True
