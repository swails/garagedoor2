""" A view that shows all codes associated with a particular user """
import logging

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views import generic

from ..models import DoorCode

LOGGER = logging.getLogger(__name__)

@method_decorator(login_required, name='get')
class AllDoorCodesView(generic.ListView):
    """ Displays a view with all of the codes belonging to a specific user """
    template_name = 'garagedoor/all_codes.html'
    context_object_name = 'user_codes'

    def get_queryset(self):
        """ Return all codes owned by this user """
        return DoorCode.objects.filter(user=self.request.user)
