""" A view to create a new code with a specific name """
#pylint: disable=attribute-defined-outside-init
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.urls import reverse_lazy
from django.views.generic.edit import FormView

from ..forms import CreateDateCodeForm
from ..models import DoorCode

@method_decorator(login_required, name='dispatch')
class CreateDateCodeView(FormView):
    """ View for creating a code to open the garage door """
    template_name = 'garagedoor/create_code.html'
    form_class = CreateDateCodeForm
    success_url = reverse_lazy('garagedoor:update_code')

    def form_valid(self, form):
        """ Create the new code if the form is valid """
        self.new_code = DoorCode(name=form.data['name'], user=self.request.user)
        self.new_code.save()
        return super().form_valid(form)

    def get_success_url(self):
        """ Redirect to update the code if successful """
        return reverse_lazy('garagedoor:update_code', kwargs=dict(pk=str(self.new_code.id)))
