""" Views for controlling the door """
from django.contrib.auth.decorators import login_required
from django.http import Http404, HttpResponseRedirect

from ..myq import MyQGarageDoor, APPID, SERVICE_URL

@login_required
def open_door(request):
    """ Opens the door """
    return _set_door(request, True)

@login_required
def close_door(request):
    """ Closes the door """
    return _set_door(request, False)

def _set_door(request, open_requested=False):
    """ Sets door open or closed """
    if request.method != 'POST':
        raise Http404("Not found")
    user = request.user
    myq = MyQGarageDoor(user.myq_username, user.myq_password, SERVICE_URL, APPID,
                        user.myq_security_token)
    myq.open_door() if open_requested else myq.close_door() # pylint: disable=expression-not-assigned
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
