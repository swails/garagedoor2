""" Main Lambda handler for garage door opening service """
#pylint: disable=too-many-instance-attributes
import enum
import logging
from functools import wraps

import requests

LOGGER = logging.getLogger(__name__)

class MyQError(Exception):
    """ Throw this when an error occurred """

class TokenExpired(MyQError):
    """ Throw this when the token is expired """

class DoorState(enum.Enum):
    """ Represents the state of the garage door """
    Open = 'open'
    Closed = 'closed'
    Unknown = 'unknown'
    Opening = 'opening'
    Closing = 'closing'

def needs_security_token(func):
    """ Wrapper around functions that require a security token """
    @wraps(func)
    def wrapper(self, *args, **kwargs):
        """ Wrapper that ensures login """
        if self._security_token is None or self._account_id is None: # pylint: disable=protected-access
            self._login() # pylint: disable=protected-access
        try:
            return func(self, *args, **kwargs)
        except TokenExpired:
            # May require another login
            self._login() # pylint: disable=protected-access
            return func(self, *args, **kwargs)
    return wrapper

APPID = 'JVM/G9Nwih5BwKgNCjLxiFUQxQijAebyyg8QUHr7JOrP+tuPb8iHfRHKwTmDzHOu'
SERVICE_URL = 'https://api.myqdevice.com'

class MyQGarageDoor:
    """ Wrapper for interacting with a MyQ Garage door opener

    Parameters
    ----------
    username : str
        The account username for login
    password : str
        The account password for login
    base_url : str
        The URL with just the base domain name for the MyQ API server
    application_id : str
        The application ID for MyQ
    security_token : str, optional
        The authorization token. If not set, a fresh one will be fetched
    """
    LOGIN_URI = '/api/v5/Login'
    ACCOUNT_ID_URI = '/api/v5/My'
    DEVICELIST_URI = '/api/v5.1/Accounts/{}/Devices'
    ACTION_URI = '/actions'

    def __init__(self, username, password, base_url, application_id, security_token=None,
                 account_id=None):
        self.username = username
        self.password = password
        self._security_token = security_token
        self._account_id = account_id
        self._opener_id = None
        self._opener_state = None
        self.session = requests.Session()
        self.base_url = base_url
        self.application_id = application_id

    def _login(self):
        """ Logs into the application """
        if 'SecurityToken' in self.session.headers:
            del self.session.headers['SecurityToken']
        response = self.session.post(self.base_url + self.LOGIN_URI,
                                     json=dict(username=self.username, password=self.password))
        body = response.json()
        try:
            response.raise_for_status()
        except requests.HTTPError as err:
            raise MyQError(body['description']) from err
        LOGGER.debug('Login body: %s', body)
        self.security_token = body['SecurityToken']
        # Now get the account ID
        response = self.session.get(self.base_url + self.ACCOUNT_ID_URI,
                                    params=dict(expand='account'))
        response.raise_for_status()
        body = response.json()
        LOGGER.debug('Account details body: %s', body)
        self._account_id = body['Account']['Id']
        self._get_opener()

    @needs_security_token
    def _get_opener(self):
        """ Gets the list of devices and what state it's in """
        devicelist_uri = self.DEVICELIST_URI.format(self.account_id)
        response = raise_for_expiry(self.session.get(self.base_url + devicelist_uri))
        response.raise_for_status()
        LOGGER.debug('Response body: %s', response.json())
        for device in response.json()['items']:
            if device['device_type'] == 'garagedooropener':
                # myq automatically redirects http -> https, but turns every method into GET. So
                # to avoid that, we need to replace http with https. If they ever fix this, though
                # and have it start with https, then we need to need to make sure we don't change
                # https to httpss (or just change it back)
                self._opener_id = device['href'].replace('http', 'https').replace('httpss', 'https')
                self._opener_state = device['state']['door_state']
        if self._opener_id is None:
            raise MyQError('Could not find garage door') # pragma: nocover

    def check_door_state(self):
        """ Returns the state of the door, either Open, Closed, Opening, Closing, or Unknown """
        self._get_opener()
        return DoorState(self._opener_state)

    @needs_security_token
    def _set_door_state(self, value):
        """ Either opens the door (value="open") or closes the door (value="closed") """
        body = dict(action_type=value)
        LOGGER.debug('Sending request body %s to %s', body, self._opener_id)
        response = raise_for_expiry(self.session.put(self._opener_id.replace('v5', 'v5.1') + self.ACTION_URI, json=body))
        response.raise_for_status()

    def open_door(self):
        """ Opens the garage door """
        self._set_door_state('open')

    def close_door(self):
        """ Closes the garage door """
        self._set_door_state('close')

    def toggle_door(self):
        " Opens the door if it's closed (or closing) and closes the door if it's open (or opening) "
        state = self.check_door_state()
        if state in (DoorState.Open, DoorState.Opening):
            self.close_door()
        elif state in (DoorState.Closed, DoorState.Closing):
            self.open_door()
        else:
            LOGGER.warning('Could not determine door state: %s. Closing door', state)
            self.close_door()

    @property
    @needs_security_token
    def security_token(self):
        """ Getter for the security token (access token) for MyQ """
        return self._security_token

    @security_token.setter
    def security_token(self, value):
        """ Setter for the security token (access token) for MyQ """
        self._security_token = self.session.headers['SecurityToken'] = value

    @property
    @needs_security_token
    def account_id(self):
        """ Account ID for this user """
        return self._account_id

    @property
    def application_id(self):
        """ Application ID for MyQ """
        return self._application_id

    @application_id.setter
    def application_id(self, value):
        """ Application ID for MyQ """
        self.session.headers['MyQApplicationId'] = self._application_id = value #pylint: disable=attribute-defined-outside-init

def raise_for_expiry(response):
    """ Checks if the token has expired """
    if response.status_code == 401 and 'expired' in response.json().get('description', '').lower():
        raise TokenExpired()
    return response
