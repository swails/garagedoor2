import os

def get_data_file_path(filename):
    return os.path.join(os.path.split(__file__)[0], 'data', filename)
