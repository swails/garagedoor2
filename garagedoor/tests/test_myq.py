""" Tests the myq class """
import json
import uuid

import responses
from django.test import TestCase

from .utils import get_data_file_path
from ..myq import MyQGarageDoor, MyQError, DoorState

class TestMyQAPIWrapper(TestCase):
    """ Tests the MyQ API Wrapper """

    VALID_APPLICATION_ID = str(uuid.uuid4())
    VALID_ACCESS_TOKEN = str(uuid.uuid4())
    BASE_URL = 'https://www.testmyq.com'
    VALID_USERNAME = 'some@guy.whatever'
    VALID_PASSWORD = 'notastrongpassword'

    @classmethod
    def login_callback(cls, request):
        found_key = False
        for key, value in request.headers.items():
            if key.lower() == 'myqapplicationid':
                if value != cls.VALID_APPLICATION_ID:
                    return (401, {}, json.dumps(_BAD_APPID))
                found_key = True
        assert found_key, 'Did not find application ID'
        payload = json.loads(request.body)
        assert 'password' in payload and 'username' in payload, 'Username/password not in payload'
        if payload['username'] != cls.VALID_USERNAME or \
                payload['password'] != cls.VALID_PASSWORD:
            return (401, {}, json.dumps(_BAD_CREDS))
        return (200, {}, json.dumps(_GOOD_LOGIN))

    @classmethod
    def _validate_headers(cls, headers):
        found_sectoken = False
        for key, value in headers.items():
            if key.lower() == 'myqapplicationid':
                assert value == cls.VALID_APPLICATION_ID
            if key.lower() == 'securitytoken':
                if value != cls.VALID_ACCESS_TOKEN:
                    return (401, {}, json.dumps(_BAD_TOKEN))
                found_sectoken = True

        assert found_sectoken, 'Unexpectedly did not find security token'

    def account_details_callback(self, request):
        self._validate_headers(request.headers)
        return (200, {}, json.dumps(_GOOD_ACCOUNT_DETAILS))

    def devicedetails_callback(self, request):
        self._validate_headers(request.headers)
        if self.simulate.lower() == 'open':
            return (200, {}, json.dumps(_GOOD_DEVICE_OPEN_DETAILS))
        elif self.simulate.lower() == 'unknown':
            return (200, {}, json.dumps(_GOOD_DEVICE_UNKNOWN_DETAILS))
        return (200, {}, json.dumps(_GOOD_DEVICE_DETAILS))

    def validate_openclose_command(self, request):
        self._validate_headers(request.headers)
        payload = json.loads(request.body)
        if self.ensure_door_opens:
            self.assertEqual(payload['action_type'], 'open')
        else:
            self.assertEqual(payload['action_type'], 'close')
        return (204, {}, b'{}')

    def setUp(self):
        content_type = 'application/json'
        responses.add_callback(responses.POST, self.BASE_URL + MyQGarageDoor.LOGIN_URI,
                               callback=self.login_callback, content_type=content_type)
        device_uri = MyQGarageDoor.DEVICELIST_URI.format(_GOOD_ACCOUNT_DETAILS['Account']['Id'])
        responses.add_callback(responses.GET, self.BASE_URL + device_uri,
                               callback=self.devicedetails_callback, content_type=content_type)
        sn = _GOOD_DEVICE_DETAILS['items'][0]['serial_number']
        url = self.BASE_URL + device_uri + '/{}'.format(sn) + MyQGarageDoor.ACTION_URI
        urlcase2 = url.replace('Account', 'account').replace('Devices', 'devices')
        responses.add_callback(responses.PUT, url, callback=self.validate_openclose_command,
                               content_type=content_type)
        responses.add_callback(responses.PUT, urlcase2, callback=self.validate_openclose_command,
                               content_type=content_type)
        responses.add_callback(responses.GET, self.BASE_URL + MyQGarageDoor.ACCOUNT_ID_URI,
                               callback=self.account_details_callback, content_type=content_type)
        self.simulate = 'closed'
        self.ensure_door_opens = True
        self.myq = MyQGarageDoor(self.VALID_USERNAME, self.VALID_PASSWORD, self.BASE_URL,
                                 self.VALID_APPLICATION_ID)

    @responses.activate
    def test_myq_login(self):
        """ Tests the MyQ Login request """
        self.myq._login()
        self.assertEqual(self.myq.security_token, self.VALID_ACCESS_TOKEN)

    @responses.activate
    def test_myq_check_door_state(self):
        """ Tests the MyQ door state checking """
        self.assertIs(self.myq.check_door_state(), DoorState.Closed)

    @responses.activate
    def test_myq_auto_refresh_stale_token(self):
        """ Tests the MyQ wrapper automatically updates invalid tokens """
        self.myq._security_token = self.myq.session.headers['SecurityToken'] = 'invalid-token'
        self.assertIs(self.myq.check_door_state(), DoorState.Closed)
        self.assertEqual(self.myq.security_token, self.VALID_ACCESS_TOKEN)

    @responses.activate
    def test_myq_open_close_closed_door(self):
        """ Tests the Open/Close/Toggle door functionality when door is closed """
        self.ensure_door_opens = True
        self.myq.open_door()
        self.ensure_door_opens = False
        self.myq.close_door()
        self.ensure_door_opens = True
        self.myq.toggle_door()

    @responses.activate
    def test_myq_open_close_open_door(self):
        """ Tests the Open/Close/Toggle door functionality when door is open """
        self.simulate = 'open'
        self.ensure_door_opens = True
        self.myq.open_door()
        self.ensure_door_opens = False
        self.myq.close_door()
        self.ensure_door_opens = False
        self.myq.toggle_door()

    @responses.activate
    def test_myq_open_close_unknown_door(self):
        """ Tests the Toggle door functionality when door is in unknown state """
        self.simulate = 'unknown'
        self.ensure_door_opens = False
        self.myq.toggle_door()

    @responses.activate
    def test_myq_open_close_door_refresh_token(self):
        """ Tests that Open/Close door updates security token if necessary """
        self.myq.security_token = 'behave like an expired token'
        self.ensure_door_opens = True
        self.myq.open_door()
        self.assertEqual(self.myq.security_token, self.VALID_ACCESS_TOKEN)

    @responses.activate
    def test_myq_login_BAD_TOKEN(self):
        """ Tests proper error handling of MyQ login with bad application ID """
        self.myq.application_id = 'invalidid'
        with self.assertRaises(MyQError):
            self.myq.check_door_state()

    @responses.activate
    def test_myq_login_bad_password(self):
        """ Tests proper error handling of MyQ Login with bad password """
        self.myq.password = 'badpassword'
        with self.assertRaises(MyQError):
            self.myq.check_door_state()

    @responses.activate
    def test_myq_login_bad_username(self):
        """ Tests proper error handling of MyQ Login with bad password """
        self.myq.username = 'badusername'
        with self.assertRaises(MyQError):
            self.myq.check_door_state()

# Responses, since they are pretty long

_BAD_APPID = dict(code='401.102', message='Unauthorized',
                  description='The current call is Unauthorized')
_BAD_CREDS = dict(code='401.203', message='Unauthorized',
                  description="The username or password you entered is incorrect. Try again.")
_GOOD_LOGIN = dict(SecurityToken=TestMyQAPIWrapper.VALID_ACCESS_TOKEN)
_BAD_TOKEN = dict(code='401.101', message="Unauthorized",
                  description='The Security Token has expired')
with open(get_data_file_path('account_details_response.json'), 'r') as f:
    _GOOD_ACCOUNT_DETAILS = json.load(f)
with open(get_data_file_path('device_response.json'), 'r') as f:
    _GOOD_DEVICE_DETAILS = json.load(f)
with open(get_data_file_path('device_response_open.json'), 'r') as f:
    _GOOD_DEVICE_OPEN_DETAILS = json.load(f)
with open(get_data_file_path('device_response_unknown.json'), 'r') as f:
    _GOOD_DEVICE_UNKNOWN_DETAILS = json.load(f)
