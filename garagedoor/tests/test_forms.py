""" Tests the forms used in the garage door code portion of the app """
import random
from datetime import date, time

from django.test import TestCase

from .. import forms
from ..models.doorcode import US_TIMEZONES

class TestDoorCodeForms(TestCase):

    def test_create_date_code_form(self):
        """ Tests the CreateDateCodeForm for creating a new date code """
        data = dict(name='Some Name', time_zone=random.choice(US_TIMEZONES))
        self.assertTrue(forms.CreateDateCodeForm(data).is_valid())

        data = dict(name='Some Name', time_zone='Not a TimeZone')
        self.assertFalse(forms.CreateDateCodeForm(data).is_valid())

    def test_update_door_code_form(self):
        """ Tests the UpdateDoorCodeForm """
        self.assertTrue(forms.UserUpdateDoorCodeForm(dict(start_date=date(2018, 1, 1))).is_valid())
        self.assertFalse(forms.UserUpdateDoorCodeForm(dict(end_date=date(2018, 1, 1))).is_valid())
        data_all_fields = dict(start_date=date(2018, 1, 1), end_date=date(2018, 5, 1),
                               start_time=time(9, 0, 0), end_time=time(16, 0, 0), repeat=5)
        self.assertTrue(forms.UserUpdateDoorCodeForm(data_all_fields).is_valid())

    def test_create_new_passcode(self):
        """ Tests the CreateNewPasscode form used by anonymous user granted access to a garage """
        self.assertTrue(
            forms.CreateNewPasscodeForm(dict(passcode='1234', confirm_passcode='1234')).is_valid()
        )

    def test_create_new_passcode_fails_with_bad_confirmation(self):
        """ Tests that CreateNewPasscodeForm fails validation when confirmation does not match """
        self.assertFalse(
            forms.CreateNewPasscodeForm(dict(passcode='1234', confirm_passcode='4321')).is_valid()
        )
