""" Tests the models for the garage door codes """
from datetime import date, datetime, time
from django.test import TestCase

from users.models import User
from .. import models
from ..models import doorcode

def _saver(thing):
    thing.save()
    return thing

class TestDoorCodeModels(TestCase):
    """ Tests the door code models to ensure they're working as expected """

    def setUp(self):
        self.user = User.objects.create_user('some_username', password='somestrongpassword')

    def test_door_code_with_no_password(self):
        """ Tests instantiating DoorCodes and checking passwords """
        code_1 = models.DoorCode(user=self.user)
        code_2 = models.DoorCode(user=self.user)

        code_1.set_passcode('01234')
        self.assertTrue(code_1.check_passcode('01234'))
        self.assertFalse(code_1.check_passcode('nottherightpasscode'))
        self.assertIs(code_2.passcode, None)

    def test_helper_functions(self):
        """ Tests the door code helper functions """
        self.assertIsInstance(doorcode.human_readable_date_string(date.today()), str)
        self.assertIsInstance(doorcode.human_readable_time_string(datetime.now().time()), str)

    def test_door_code_active_date(self):
        """ Tests the DoorCodeActiveDate model with start date and end date """
        code = models.DoorCode(user=self.user)
        code.save()
        active_date1 = _saver(models.DoorCodeActiveDate(code=code, start_date=date(2018, 1, 1)))
        active_date2 = _saver(models.DoorCodeActiveDate(code=code, start_date=date(2018, 2, 1),
                                                        end_date=date(2018, 3, 1)))
        active_date3 = _saver(models.DoorCodeActiveDate(code=code, start_date=date(2018, 2, 1),
                                                        end_date=date(2018, 3, 1), repeat=7))
        active_date4 = _saver(models.DoorCodeActiveDate(code=code, start_date=date(2018, 2, 1),
                                                        end_date=date(2018, 3, 1), repeat=7,
                                                        start_time=time(4, 1, 1),
                                                        end_time=time(5, 1, 2)))
        # Ensure these combinations do not result in a validation error
        _saver(models.DoorCodeActiveDate(code=code, start_date=date(2018, 2, 1),
                                         repeat=7, start_time=time(4, 1, 1),
                                         end_time=time(5, 1, 2)))
        _saver(models.DoorCodeActiveDate(code=code, start_date=date(2018, 2, 1),
                                         start_time=time(4, 1, 1),
                                         end_time=time(5, 1, 2)))

        self.assertEqual(active_date1.repeat_string, '')
        self.assertEqual(active_date3.repeat_string, '7 days')
        self.assertEqual(
            active_date1.date_range_string,
            '{} \u2014'.format(doorcode.human_readable_date_string(active_date1.start_date))
        )
        self.assertEqual(
            active_date2.date_range_string,
            '{} \u2014 {}'.format(doorcode.human_readable_date_string(active_date2.start_date),
                                  doorcode.human_readable_date_string(active_date2.end_date))
        )
        self.assertEqual(active_date1.time_range_string, 'All Day')
        self.assertEqual(
            active_date4.time_range_string,
            '{} \u2014 {}'.format(doorcode.human_readable_time_string(active_date4.start_time),
                                  doorcode.human_readable_time_string(active_date4.end_time))
        )
