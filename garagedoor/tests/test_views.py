""" Tests the various views of the garage door code management """
import random
from datetime import timedelta, date, time, datetime
from string import ascii_letters
from unittest import mock
from uuid import uuid4

import pytz
from django.contrib.auth.models import AnonymousUser
from django.http import Http404
from django.test import RequestFactory, TestCase
from django.utils import timezone

from garagedoor2.tests import TestCaseWithUser
from users.models import User

from ..forms import UserUpdateDoorCodeForm, SubmitPasscodeForm, CreateNewPasscodeForm
from ..models import DoorCode, DoorCodeUsed, DoorCodeActiveDate
from ..views import (AllDoorCodesView, CreateDateCodeView, UpdateUseCodeView, DoorCodeHistoryView,
                     delete_date_active_code, delete_code, confirm_code, clear_code, doorcontrol)
from ..views.updatecode import CodeCheckReturnValues, check_dates_and_toggle_door
from .. import myq

class TwoUsersTestCase(TestCaseWithUser):
    """ Mixin to automatically create two users for each test """
    factory = RequestFactory()

    @staticmethod
    def _create_code(user, name):
        from ..models.doorcode import US_TIMEZONES
        code = DoorCode(user=user, name=name, timezone=US_TIMEZONES[0])
        code.save()
        return code

    def setUp(self):
        super().setUp()
        self.user1 = self.create_user('me@email.com', 'password1', 'John Q.', 'Tester',
                                      'me@email.com', 'password1myq')
        self.user2 = self.create_user('you@email.com', 'password1', 'Jane S.', 'User',
                                      'you@email.com', 'something-dark-side')

class TestCreateDoorCodeView(TwoUsersTestCase):
    """ Test the form for creating a door code """

    def setUp(self):
        from ..models.doorcode import US_TIMEZONES
        super().setUp()
        self.timezone = US_TIMEZONES[0]
        self.code_name = 'First Code'
        self.data = dict(name=self.code_name, time_zone=self.timezone)

    def test_create_door_code_requires_login(self):
        """ Ensure that creating a code requires you to be logged in """
        self.assertEqual(len(DoorCode.objects.all()), 0)
        request = self.factory.post('/doorcontrol/create', data=self.data)
        request.user = AnonymousUser()

        CreateDateCodeView.as_view()(request)

        self.assertEqual(len(DoorCode.objects.all()), 0)

    def test_create_door_code_succeeds(self):
        """ Ensure that a date/door code can be created by an authenticated user """
        self.assertEqual(len(DoorCode.objects.all()), 0)
        request = self.factory.post('/doorcontrol/create', data=self.data)
        request.user = self.user1

        CreateDateCodeView.as_view()(request)

        self.assertEqual(len(DoorCode.objects.all()), 1)
        self.assertEqual(DoorCode.objects.all()[0].name, 'First Code')

class TestAllDoorCodesView(TwoUsersTestCase):
    """ Tests the AllDoorCodesView class """

    def setUp(self):
        super().setUp()
        self.doorcode_1_user_1 = self._create_code(self.user1, 'User 1 Code 1')
        self.doorcode_2_user_1 = self._create_code(self.user1, 'User 1 Code 2')
        self.doorcode_1_user_2 = self._create_code(self.user2, 'User 2 Code 1')
        self.doorcode_2_user_2 = self._create_code(self.user2, 'User 2 Code 2')

    def test_all_code_view_requires_login(self):
        """ Ensure that viewing all codes requires login """
        request = self.factory.get('/doorcontrol')
        request.user = AnonymousUser()

        response = AllDoorCodesView.as_view()(request)
        self.assertGreaterEqual(response.status_code, 300)

    def test_all_code_view_returns_owned_codes(self):
        """ Ensure that user can only view their own codes """
        request = self.factory.get('/doorcontrol')
        request.user = self.user1

        view = AllDoorCodesView(request=request)

        queryset = view.get_queryset()

        self.assertEqual(len(queryset.all()), 2)
        my_user_codes = {self.doorcode_1_user_1.id, self.doorcode_2_user_1.id}
        self.assertEqual({obj.id for obj in queryset.all()}, my_user_codes)
        self.assertNotIn(self.doorcode_1_user_2.id, my_user_codes)
        self.assertNotIn(self.doorcode_2_user_2.id, my_user_codes)

class TestDeleteCodes(TwoUsersTestCase):
    """ Tests the deletion of active date codes """

    def setUp(self):
        super().setUp()
        self.doorcode_1 = self._create_code(self.user1, 'Code 1')
        self.doorcode_2 = self._create_code(self.user1, 'Code 2')
        self.active_time_1 = DoorCodeActiveDate(code=self.doorcode_1,
                                                start_date=date.today(),
                                                repeat=14,
                                                start_time=time(9, 0, 0),
                                                end_time=time(16, 0, 0))
        self.active_time_1.save()
        self.active_time_2 = DoorCodeActiveDate(code=self.doorcode_1,
                                                start_date=date.today(),
                                                repeat=4,
                                                start_time=time(9, 0, 0),
                                                end_time=time(16, 0, 0))
        self.active_time_2.save()

    def test_delete_requires_post(self):
        """ Test that trying to delete an active time fails if you are anonymous """
        request = self.factory.post('/doorcode/{}/activation/{}/delete'.format(
            str(self.doorcode_1.id), str(self.active_time_1.id)
        ))
        request.user = AnonymousUser()

        with self.assertRaises(Http404):
            delete_date_active_code(request, str(self.doorcode_1.id), str(self.active_time_1.id))

    def test_delete_requires_valid_code(self):
        """ Test that trying to delete an active time fails if IDs are incorrect """
        request = self.factory.post('/doorcode/{}/activation/{}/delete'.format(
            str(self.doorcode_1.id), str(self.active_time_1.id)
        ))
        request.user = self.user1

        with self.assertRaises(Http404):
            delete_date_active_code(request, str(uuid4()), str(self.active_time_1.id))

        with self.assertRaises(Http404):
            delete_date_active_code(request, str(self.doorcode_1.id), '1000')

    def test_delete_requires_owner(self):
        """ Test that trying to delete an active time fails if you do not own the code """
        request = self.factory.post('/doorcode/{}/activation/{}/delete'.format(
            str(self.doorcode_1.id), str(self.active_time_1.id)
        ))
        request.user = self.user2

        with self.assertRaises(Http404):
            delete_date_active_code(request, str(self.doorcode_1.id), str(self.active_time_1.id))

    def test_delete_active_time_succeeds(self):
        """ Test that deleting an active time attached to a date code succeeds """
        self.assertEqual(len(DoorCodeActiveDate.objects.all()), 2)
        request = self.factory.post('/doorcode/{}/activation/{}/delete'.format(
            str(self.doorcode_1.id), str(self.active_time_1.id)
        ))
        request.user = self.user1

        delete_date_active_code(request, str(self.doorcode_1.id), str(self.active_time_1.id))

        self.assertEqual(len(DoorCodeActiveDate.objects.all()), 1)

    def test_delete_code_fails_as_anonymous(self):
        """ Ensure deleting a code fails for an anonymous user """
        self._check_delete_code_fails_for_user(AnonymousUser())

    def test_delete_code_fails_as_non_owner(self):
        """ Ensure that only the owner can delete a code """
        self._check_delete_code_fails_for_user(self.user2)

    def test_delete_code_fails_if_code_does_not_exist(self):
        """ Ensure that delete_code returns 404 if the code does not exist """
        self._check_delete_code_fails_for_user(self.user1, doorcode_id=uuid4())

    def _check_delete_code_fails_for_user(self, user, doorcode_id=None):
        request = self.factory.post('/doorcode/{}/delete')
        request.user = user

        if doorcode_id is None:
            doorcode_id = self.doorcode_1.id
        self.assertEqual(len(DoorCode.objects.all()), 2)
        with self.assertRaises(Http404):
            delete_code(request, str(doorcode_id))
        self.assertEqual(len(DoorCode.objects.all()), 2)

    def test_delete_code_works(self):
        """ Ensure that delete_code works and cascades to active dates """
        request = self.factory.post('/doorcode/{}/delete')
        request.user = self.user1

        self.assertEqual(len(DoorCode.objects.all()), 2)
        self.assertEqual(len(DoorCodeActiveDate.objects.all()), 2)
        delete_code(request, str(self.doorcode_1.id))
        self.assertEqual(len(DoorCode.objects.all()), 1)
        self.assertEqual(len(DoorCodeActiveDate.objects.all()), 0) # cascade deletes

class TestUpdateUseCodeView(TwoUsersTestCase):
    """ Tests the UpdateUseCodeView """

    def setUp(self):
        super().setUp()
        self.doorcode_1 = self._create_code(self.user1, 'Code 1')
        self.doorcode_2 = self._create_code(self.user1, 'Code 2')

    def test_anonymous_user_can_set_code(self):
        """ Tests that an anonymous user can set a code's passcode """
        self._check_set_code_for_user(AnonymousUser())

    def test_user2_can_set_code(self):
        """ Tests that non-owner can set a code's passcode """
        self._check_set_code_for_user(self.user2)

    def _check_set_code_for_user(self, user):
        self.assertFalse(self.doorcode_1.passcode)
        url = '/doorcode/{}/'.format(self.doorcode_1.id)
        request = self.factory.get(url)
        request.user = user

        # The routing framework adds the URL path parameters as kwargs. Since we we're calling the
        # view (and instantiating the ViewClass) directly, we have to do that ourselves here
        response = UpdateUseCodeView.as_view()(request, pk=str(self.doorcode_1.id))

        self.assertEqual(response.status_code, 200)

        # Now POST with the form setting the passcode.
        data = dict(passcode='some-pin', confirm_passcode='does-not-match')
        request = self.factory.post(url, data=data)
        request.user = AnonymousUser()

        response = UpdateUseCodeView.as_view()(request, pk=str(self.doorcode_1.id))

        self.doorcode_1.refresh_from_db()

        self.assertFalse(self.doorcode_1.passcode)

        data = dict(passcode='some-pin', confirm_passcode='some-pin')
        request = self.factory.post(url, data=data)
        request.user = AnonymousUser()

        UpdateUseCodeView.as_view()(request, pk=str(self.doorcode_1.id))

        self.doorcode_1.refresh_from_db()

        self.assertTrue(self.doorcode_1.passcode)
        self.assertTrue(self.doorcode_1.check_passcode('some-pin'))
        view = UpdateUseCodeView(request=request, kwargs=dict(pk=str(self.doorcode_1.id)))
        self.assertEqual(len(view.get_template_names()), 1)
        context = view.get_context_data(newarg=1)
        # Ensure that form_valid "works" and that it doesn't change the PIN
        view.form_valid(view.get_form_class()({'passcode': 'diffpin', 'confirm_passcode': 'diffpin'}))
        self.assertTrue(self.doorcode_1.check_passcode('some-pin'))
        self.assertIs(context['code'], view.my_code)
        self.assertEqual(context['owner'].id, self.doorcode_1.user.id)
        self.assertEqual(context['newarg'], 1)

    def test_owner_can_update_code(self):
        """ Test that the owner can update their code """
        data = dict(start_date_year='2019', start_date_month='1', start_date_day='1', repeat=14,
                    end_date_year='2019', end_date_month='2', end_date_day='1',
                    start_time=time(9, 0, 0), end_time=time(16, 0, 0))
        request = self.factory.post('/doorcode/{}'.format(str(self.doorcode_1.id)), data=data)
        request.user = self.user1

        view = UpdateUseCodeView(request=request, kwargs=dict(pk=str(self.doorcode_1.id)))

        self.assertIs(view.get_form_class(), UserUpdateDoorCodeForm)
        # Make sure the initial items are present *and* that all keys are inside 'data'
        self.assertTrue(view.get_initial())
        self.assertEqual(len(view.get_template_names()), 1)
        context = view.get_context_data()
        # No active times present yet
        self.assertEqual(len(context['code_active_times']), 0)
        UpdateUseCodeView.as_view()(request, pk=str(self.doorcode_1.id))

        self.doorcode_1.refresh_from_db()
        self.assertEqual(len(DoorCodeActiveDate.objects.filter(code=self.doorcode_1).all()), 1)

        # Now create a schedule with no end-date or repeat
        data = dict(start_date_year='2020', start_date_month='1', start_date_day='1',
                    start_time=time(9, 0, 0), end_time=time(16, 0, 0))
        request = self.factory.post('/doorcode/{}'.format(str(self.doorcode_2.id)), data=data)
        request.user = self.user1
        UpdateUseCodeView.as_view()(request, pk=str(self.doorcode_2.id))

    def test_update_end_date(self):
        """ Test that updating with end-date not being numeric is handled properly """
        data = dict(start_date_year='2019', start_date_month='1', start_date_day='1', repeat=14,
                    end_date_year='', end_date_month='', end_date_day='',
                    start_time=time(9, 0, 0), end_time=time(16, 0, 0))
        request = self.factory.post('/doorcode/{}'.format(str(self.doorcode_1.id)), data=data)
        request.user = self.user1

        view = UpdateUseCodeView(request=request, kwargs=dict(pk=str(self.doorcode_1.id)))

        self.assertIs(view.get_form_class(), UserUpdateDoorCodeForm)
        # Make sure the initial items are present *and* that all keys are inside 'data'
        self.assertTrue(view.get_initial())
        self.assertEqual(len(view.get_template_names()), 1)
        context = view.get_context_data()
        # No active times present yet
        self.assertEqual(len(context['code_active_times']), 0)
        UpdateUseCodeView.as_view()(request, pk=str(self.doorcode_1.id))

        self.doorcode_1.refresh_from_db()
        self.assertEqual(len(DoorCodeActiveDate.objects.filter(code=self.doorcode_1).all()), 1)

class TestClearCode(TwoUsersTestCase):
    """ Tests clearing a passcode that's been set """

    def setUp(self):
        super().setUp()
        self.doorcode_1 = self._create_code(self.user1, 'Code 1')
        self.doorcode_1.set_passcode('some-pin')
        self.doorcode_1.save()
        self.assertTrue(self.doorcode_1.check_passcode('some-pin'))

    def test_clear_requires_post(self):
        """ Ensure that the clear end-point requires a POST """
        request = self.factory.get('/doorcode/{}/clear'.format(str(self.doorcode_1.id)))
        request.user = self.user1

        with self.assertRaises(Http404):
            clear_code(request, str(self.doorcode_1.id))

    def test_anonymous_cannot_clear(self):
        """ Tests that an anonymous user cannot clear the code """
        request = self.factory.post('doorcode/{}/clear'.format(str(self.doorcode_1.id)))
        request.user = AnonymousUser()
        with self.assertRaises(Http404):
            clear_code(request, str(self.doorcode_1.id))
        self.assertTrue(self.doorcode_1.passcode)

    def test_non_owner_cannot_clear(self):
        """ Tests that only the owner can clear the code """
        request = self.factory.post('doorcode/{}/clear'.format(str(self.doorcode_1.id)))
        request.user = self.user2
        with self.assertRaises(Http404):
            clear_code(request, str(self.doorcode_1.id))
        self.assertTrue(self.doorcode_1.passcode)

    def test_owner_can_clear_code(self):
        """ Tests that the owner of a code can clear it """
        request = self.factory.post('doorcode/{}/clear'.format(str(self.doorcode_1.id)))
        request.user = self.user1
        response = clear_code(request, str(self.doorcode_1.id))
        self.doorcode_1.refresh_from_db()
        self.assertFalse(self.doorcode_1.passcode)

class TestShowUsageHistory(TwoUsersTestCase):
    """ Tests the view showing code usage """

    def setUp(self):
        super().setUp()
        self.doorcode_1 = self._create_code(self.user1, 'Code 1')
        self.code_used_1 = DoorCodeUsed(code=self.doorcode_1, time=datetime.now(),
                                        state='Closed', valid=True)
        self.code_used_2 = DoorCodeUsed(code=self.doorcode_1, time=datetime.now(),
                                        state='Opened', valid=True)
        self.code_used_3 = DoorCodeUsed(code=self.doorcode_1, time=datetime.now(),
                                        state='Not Active', valid=True)
        self.code_used_4 = DoorCodeUsed(code=self.doorcode_1, time=datetime.now(),
                                        state='Bad Code', valid=True)
        self.code_used_5 = DoorCodeUsed(code=self.doorcode_1, time=datetime.now(),
                                        state='Internal Error', valid=True)
        self.code_used_1.save()
        self.code_used_2.save()
        self.code_used_3.save()
        self.code_used_4.save()
        self.code_used_5.save()

    def test_requires_login(self):
        """ Tests that the usage history showing requires login """
        request = self.factory.get('/doorcode/{}/history'.format(str(self.doorcode_1.id)))
        request.user = AnonymousUser()

        response = DoorCodeHistoryView.as_view()(request, pk=str(self.doorcode_1.id))
        self.assertGreaterEqual(response.status_code, 300)
        self.assertIn('/users/login', response.url)

    def test_only_works_for_owner(self):
        """ Tests that the usage history showing does not work for non-owner """
        request = self.factory.get('/doorcode/{}/history'.format((str(self.doorcode_1.id))))
        request.user = self.user2

        with self.assertRaises(Http404):
            DoorCodeHistoryView.as_view()(request, pk=str(self.doorcode_1.id))

    def test_owner_can_view_history(self):
        """ Ensure that an owner can view their own code use history """
        request = self.factory.get('/doorcode/{}/history'.format((str(self.doorcode_1.id))))
        request.user = self.user1

        response = DoorCodeHistoryView.as_view()(request, pk=str(self.doorcode_1.id))
        self.assertEqual(response.status_code, 200)
        rendered_content = response.render().content.decode('utf-8')
        self.assertIn('Code 1', rendered_content)
        self.assertIn('Closed', rendered_content)
        self.assertIn('Opened', rendered_content)
        self.assertIn('Internal Error', rendered_content)
        self.assertIn('Bad Code', rendered_content)

class TestCodeActiveChecking(TestCase):

    factory = RequestFactory()

    def setUp(self):
        super().setUp()
        self.passcode = ''.join([random.choice(ascii_letters) for i in range(10)])

        self.user = User.objects.create_user(username='codeowner', password='password')
        self.user.myq_username = 'some_myq_username'
        self.user.myq_password = 'some_myq_password'
        self.user.myq_security_token = 'token_1'
        self.user.save()
        self.user.refresh_from_db()

        self.code = DoorCode(user=self.user, name='Test Code')
        self.code.set_passcode(self.passcode)
        self.code.save()
        self.code.refresh_from_db()

        self.now = timezone.now().astimezone(pytz.timezone(self.code.timezone))

    def _create_active_time(self, start_date, **kwargs):
        """ Creates a DoorCodeActiveDate. kwargs can be any arguments to that model """
        active_date = DoorCodeActiveDate(code=self.code, start_date=start_date, **kwargs)
        active_date.save()
        active_date.refresh_from_db()
        return active_date

    def _mock_myq_api_wrapper(self, current_door_state, can_connect=True, security_token=None):
        """ Mocks the myq Check Door State function

        Parameters
        ----------
        return_state : myq.DoorState
            The door state to return from check_door_state
        can_connect : bool, optional, default=True
            If True, check_door_state returns successfully. Otherwise it raises MyQError
        """
        assert isinstance(current_door_state, myq.DoorState)
        if not can_connect:
            def side_effect():
                raise myq.MyQError('Cannot connect to service')
        else:
            side_effect = None
        myq.MyQGarageDoor.check_door_state = mock.MagicMock(return_value=current_door_state,
                                                            side_effect=side_effect)
        myq.MyQGarageDoor.toggle_door = mock.MagicMock(return_value=None)
        security_token = security_token or self.user.myq_security_token
        myq.MyQGarageDoor.security_token = mock.PropertyMock(return_value=security_token)

    def test_no_active_times(self):
        """ Test that no active times always returns code inactive response """
        self.assertEqual(check_dates_and_toggle_door(self.code), CodeCheckReturnValues.NOT_ACTIVE)

    def test_active_time_start_date_only_works(self):
        """ Tests that an Active start with only a start_date works only on that date """
        # Create an active time *today*
        active_date = self._create_active_time(start_date=self.now.date())

        self._mock_myq_api_wrapper(myq.DoorState.Open)
        self.assertEqual(check_dates_and_toggle_door(self.code), CodeCheckReturnValues.CLOSED_DOOR)

        self._mock_myq_api_wrapper(myq.DoorState.Opening)
        self.assertEqual(check_dates_and_toggle_door(self.code), CodeCheckReturnValues.CLOSED_DOOR)

        self._mock_myq_api_wrapper(myq.DoorState.Closed)
        self.assertEqual(check_dates_and_toggle_door(self.code), CodeCheckReturnValues.OPENED_DOOR)

        self._mock_myq_api_wrapper(myq.DoorState.Closing)
        self.assertEqual(check_dates_and_toggle_door(self.code), CodeCheckReturnValues.OPENED_DOOR)

        self._mock_myq_api_wrapper(myq.DoorState.Unknown)
        self.assertEqual(check_dates_and_toggle_door(self.code), CodeCheckReturnValues.CLOSED_DOOR)

        # Check that we can't get in before our start date
        active_date.start_date = (self.now + timedelta(days=10)).date()
        active_date.save()
        self.assertEqual(check_dates_and_toggle_door(self.code), CodeCheckReturnValues.NOT_ACTIVE)

    def test_handle_cannot_connect(self):
        """ Tests that inability to connect to MyQ is properly handled """
        self._create_active_time(start_date=self.now.date())
        self._mock_myq_api_wrapper(myq.DoorState.Open, can_connect=False)
        self.assertEqual(check_dates_and_toggle_door(self.code),
                         CodeCheckReturnValues.UNABLE_TO_CONNECT)

    def test_active_time_start_date_only_fails_on_another_day(self):
        """ Tests that an active start with only a start_date works only on that day """
        # Make lots of active date times, but none on today. They should all fail
        for i in range(1, 500):
            start_date = self.now.date() - i * timedelta(days=i)
            self._create_active_time(start_date=start_date)
        self.assertEqual(check_dates_and_toggle_door(self.code), CodeCheckReturnValues.NOT_ACTIVE)

    def test_check_status_updates_security_code(self):
        """ Tests that the security code is updated if MyQ needs to fetch a new one """
        self._create_active_time(start_date=self.now.date())

        new_token = 'foobar'
        # Make sure our current token is *not* set to new_token in the beginning
        self.user.refresh_from_db()
        self.assertNotEqual(self.user.myq_security_token, new_token)

        self._mock_myq_api_wrapper(myq.DoorState.Open, security_token=new_token)
        self.assertEqual(check_dates_and_toggle_door(self.code), CodeCheckReturnValues.CLOSED_DOOR)

        self.user.refresh_from_db()
        self.assertEqual(self.user.myq_security_token, new_token)

    def test_active_time_inside_time_interval_works(self):
        """ Tests that being inside the time interval *and* on an allowed day permits entry """
        start = self.now - timedelta(minutes=5)
        end = self.now + timedelta(minutes=1)
        active_date = self._create_active_time(start_date=self.now.date(), start_time=start.time(),
                                               end_time=end.time())
        self._mock_myq_api_wrapper(myq.DoorState.Open)
        self.assertEqual(check_dates_and_toggle_door(self.code), CodeCheckReturnValues.CLOSED_DOOR)
        # Now make the day wrong
        active_date.start_date = (self.now - timedelta(days=4)).date()
        active_date.save()
        self.assertEqual(check_dates_and_toggle_door(self.code), CodeCheckReturnValues.NOT_ACTIVE)

    def test_active_time_outside_time_interval_fails(self):
        """ Tests that access is denied if it is outside the allowed time """
        start = self.now - timedelta(minutes=2)
        end = self.now - timedelta(minutes=1)
        self._create_active_time(start_date=self.now.date(), start_time=start.time(),
                                 end_time=end.time())
        self._mock_myq_api_wrapper(myq.DoorState.Open)
        self.assertEqual(check_dates_and_toggle_door(self.code), CodeCheckReturnValues.NOT_ACTIVE)

    def test_active_time_repeat_days_works(self):
        """ Tests the repeat functionality and ensure it works only on multiples of the day """
        active_date = self._create_active_time(start_date=self.now.date(), repeat=10)
        self._mock_myq_api_wrapper(myq.DoorState.Closed)
        self.assertEqual(check_dates_and_toggle_door(self.code), CodeCheckReturnValues.OPENED_DOOR)

        active_date.start_date = (self.now - timedelta(days=8)).date()
        active_date.save()
        self.assertEqual(check_dates_and_toggle_door(self.code), CodeCheckReturnValues.NOT_ACTIVE)

        active_date.start_date = (self.now - timedelta(days=10)).date()
        active_date.save()
        self.assertEqual(check_dates_and_toggle_door(self.code), CodeCheckReturnValues.OPENED_DOOR)

        active_date.start_date = (self.now - timedelta(days=18)).date()
        active_date.save()
        self.assertEqual(check_dates_and_toggle_door(self.code), CodeCheckReturnValues.NOT_ACTIVE)

        active_date.start_date = (self.now - timedelta(days=20)).date()
        active_date.save()
        self.assertEqual(check_dates_and_toggle_door(self.code), CodeCheckReturnValues.OPENED_DOOR)

    def test_active_time_with_end_date(self):
        """ Tests the proper behavior of setting an end-date on active times """
        start_date = (self.now - timedelta(days=10)).date()
        end_date = (self.now + timedelta(days=10)).date()
        active_date = self._create_active_time(start_date=start_date, end_date=end_date)
        self._mock_myq_api_wrapper(myq.DoorState.Closing)
        self.assertEqual(check_dates_and_toggle_door(self.code), CodeCheckReturnValues.OPENED_DOOR)

        active_date.end_date = (self.now - timedelta(days=1)).date()
        active_date.save()
        self.assertEqual(check_dates_and_toggle_door(self.code), CodeCheckReturnValues.NOT_ACTIVE)

        active_date.start_time = (self.now - timedelta(minutes=10)).time()
        active_date.end_time = (self.now + timedelta(minutes=10)).time()
        active_date.save()
        self.assertEqual(check_dates_and_toggle_door(self.code), CodeCheckReturnValues.NOT_ACTIVE)

        active_date.end_date = (self.now + timedelta(days=10)).date()
        active_date.save()
        self.assertEqual(check_dates_and_toggle_door(self.code), CodeCheckReturnValues.OPENED_DOOR)

        active_date.end_time = (self.now - timedelta(minutes=1)).time()
        active_date.save()
        self.assertEqual(check_dates_and_toggle_door(self.code), CodeCheckReturnValues.NOT_ACTIVE)

    def test_active_time_with_end_date_time_intervals_and_repeat_days(self):
        """ Tests proper behavior of setting an end-date, interval, and repeat on active times """
        start_date = (self.now - timedelta(days=10)).date()
        end_date = (self.now + timedelta(days=10)).date()
        active_date = self._create_active_time(start_date=start_date, end_date=end_date, repeat=2)
        self._mock_myq_api_wrapper(myq.DoorState.Closing)
        self.assertEqual(check_dates_and_toggle_door(self.code), CodeCheckReturnValues.OPENED_DOOR)

        active_date.end_date = (self.now - timedelta(days=1)).date()
        active_date.save()
        self.assertEqual(check_dates_and_toggle_door(self.code), CodeCheckReturnValues.NOT_ACTIVE)

        active_date.start_time = (self.now - timedelta(minutes=10)).time()
        active_date.end_time = (self.now + timedelta(minutes=10)).time()
        active_date.save()
        self.assertEqual(check_dates_and_toggle_door(self.code), CodeCheckReturnValues.NOT_ACTIVE)

        active_date.end_date = (self.now + timedelta(days=10)).date()
        active_date.save()
        self.assertEqual(check_dates_and_toggle_door(self.code), CodeCheckReturnValues.OPENED_DOOR)

        active_date.end_time = (self.now - timedelta(minutes=1)).time()
        active_date.save()
        self.assertEqual(check_dates_and_toggle_door(self.code), CodeCheckReturnValues.NOT_ACTIVE)

        active_date.end_time = (self.now + timedelta(minutes=10)).time()
        active_date.save()
        self.assertEqual(check_dates_and_toggle_door(self.code), CodeCheckReturnValues.OPENED_DOOR)

        active_date.start_date = (self.now - timedelta(days=9)).date()
        active_date.save()
        self.assertEqual(check_dates_and_toggle_door(self.code), CodeCheckReturnValues.NOT_ACTIVE)

    def test_active_time_with_repeat_days_and_time_interval(self):
        """ Tests that codes are active only on days specified at time intervals allowed """
        start_time = (self.now - timedelta(minutes=10)).time()
        end_time = (self.now + timedelta(minutes=10)).time()
        active_date = self._create_active_time(start_date=self.now.date(), repeat=10,
                                               start_time=start_time, end_time=end_time)
        self._mock_myq_api_wrapper(myq.DoorState.Closed)
        self.assertEqual(check_dates_and_toggle_door(self.code), CodeCheckReturnValues.OPENED_DOOR)

        active_date.start_date = (self.now - timedelta(days=8)).date()
        active_date.save()
        self.assertEqual(check_dates_and_toggle_door(self.code), CodeCheckReturnValues.NOT_ACTIVE)

        active_date.start_date = (self.now - timedelta(days=10)).date()
        active_date.save()
        self.assertEqual(check_dates_and_toggle_door(self.code), CodeCheckReturnValues.OPENED_DOOR)

        active_date.start_date = (self.now - timedelta(days=18)).date()
        active_date.save()
        self.assertEqual(check_dates_and_toggle_door(self.code), CodeCheckReturnValues.NOT_ACTIVE)

        active_date.start_date = (self.now - timedelta(days=20)).date()
        active_date.save()
        self.assertEqual(check_dates_and_toggle_door(self.code), CodeCheckReturnValues.OPENED_DOOR)

        active_date.end_time = (self.now - timedelta(minutes=1)).time()
        active_date.save()
        self.assertEqual(check_dates_and_toggle_door(self.code), CodeCheckReturnValues.NOT_ACTIVE)

    def _make_request(self, passcode=None):
        data = dict(passcode=passcode or self.passcode)
        request = self.factory.post('/doorcode/some-key/submit', data=data)
        request.user = AnonymousUser()
        return request

    def test_door_code_used_creation_valid_open(self):
        """ Tests that the DoorCodeUsed object gets created for valid uses on open door """
        self._create_active_time(start_date=self.now.date())
        self._mock_myq_api_wrapper(myq.DoorState.Open)
        self.assertEqual(len(DoorCodeUsed.objects.filter(code=self.code)), 0)
        request = self._make_request()
        confirm_code(request, self.code.id)
        self.assertEqual(len(DoorCodeUsed.objects.filter(code=self.code)), 1)
        used = DoorCodeUsed.objects.get(code=self.code)
        self.assertTrue(used.valid)
        self.assertIsNone(used.reason)
        self.assertEqual(used.state, 'Closed')

    def test_door_code_used_creation_valid_closed(self):
        """ Tests that the DoorCodeUsed object gets created for valid uses on closed door """
        self._create_active_time(start_date=self.now.date())
        self._mock_myq_api_wrapper(myq.DoorState.Closed)
        self.assertEqual(len(DoorCodeUsed.objects.filter(code=self.code)), 0)
        request = self._make_request()
        confirm_code(request, self.code.id)
        self.assertEqual(len(DoorCodeUsed.objects.filter(code=self.code)), 1)
        used = DoorCodeUsed.objects.get(code=self.code)
        self.assertTrue(used.valid)
        self.assertIsNone(used.reason)
        self.assertEqual(used.state, 'Opened')

    def test_door_code_used_creation_not_active(self):
        """ Tests that the DoorCodeUsed object gets created when code is not active """
        self._create_active_time(start_date=(self.now - timedelta(days=10)).date())
        self._mock_myq_api_wrapper(myq.DoorState.Closed)
        self.assertEqual(len(DoorCodeUsed.objects.filter(code=self.code)), 0)
        request = self._make_request()
        confirm_code(request, self.code.id)
        self.assertEqual(len(DoorCodeUsed.objects.filter(code=self.code)), 1)
        used = DoorCodeUsed.objects.get(code=self.code)
        self.assertFalse(used.valid)
        self.assertEqual(used.reason, 'Not Active')

    def test_door_code_used_creation_bad_code(self):
        """ Tests that the DoorCodeUsed object gets created when the code is bad """
        self._create_active_time(start_date=self.now.date())
        self._mock_myq_api_wrapper(myq.DoorState.Closed)
        self.assertEqual(len(DoorCodeUsed.objects.filter(code=self.code)), 0)
        request = self._make_request(passcode='not the right passcode')
        confirm_code(request, self.code.id)
        self.assertEqual(len(DoorCodeUsed.objects.filter(code=self.code)), 1)
        used = DoorCodeUsed.objects.get(code=self.code)
        self.assertFalse(used.valid)
        self.assertEqual(used.reason, 'Bad Code')

    def test_door_code_used_creation_internal_error(self):
        """ Tests that the DoorCodeUsed object gets created when MyQ is not reachable """
        self._create_active_time(start_date=self.now.date())
        self._mock_myq_api_wrapper(myq.DoorState.Closed, can_connect=False)
        self.assertEqual(len(DoorCodeUsed.objects.filter(code=self.code)), 0)
        request = self._make_request()
        confirm_code(request, self.code.id)
        self.assertEqual(len(DoorCodeUsed.objects.filter(code=self.code)), 1)
        used = DoorCodeUsed.objects.get(code=self.code)
        self.assertFalse(used.valid)
        self.assertEqual(used.reason, 'Internal Error')

    def test_confirm_code_requires_post(self):
        """ Test that confirm_code requires a POST """
        self._mock_myq_api_wrapper(myq.DoorState.Closed)
        request = self.factory.get('/some/endpoint/doesnot/matter')
        request.user = AnonymousUser()
        with self.assertRaises(Http404):
            confirm_code(request, self.code.id)

class TestAdminOpenCloseWorks(TestCaseWithUser):
    """ Ensures that open/close works on the nav-bar """

    factory = RequestFactory()

    def setUp(self):
        from ..views import doorcontrol
        self.user = self.create_user('foo', 'bar', 'Foo', 'Bar', 'myquser', 'myqpass')
        class MockMyQGarageDoor:
            def __init__(self2, username, password, *args, **kwargs):
                self.assertEqual(username, 'myquser')
                self.assertEqual(password, 'myqpass')
        MockMyQGarageDoor.open_door = mock.Mock()
        MockMyQGarageDoor.close_door = mock.Mock()
        doorcontrol.MyQGarageDoor = MockMyQGarageDoor

    def _make_request(self, user):
        request = self.factory.post('/doorcode/open')
        request.user = user
        return request

    def test_open_door(self):
        """ Tests open door used by top nav-bar link """
        doorcontrol.open_door(self._make_request(self.user))
        doorcontrol.MyQGarageDoor.open_door.assert_called_once()
        doorcontrol.MyQGarageDoor.close_door.assert_not_called()

    def test_close_door(self):
        """ Tests close door used by top nav-bar link """
        doorcontrol.close_door(self._make_request(self.user))
        doorcontrol.MyQGarageDoor.open_door.assert_not_called()
        doorcontrol.MyQGarageDoor.close_door.assert_called_once()

    def test_get_fails(self):
        """ Tests that opening the door with a get request fails """
        request = self.factory.get('/doorcode/open')
        request.user = self.user
        with self.assertRaises(Http404):
            doorcontrol.open_door(request)
        with self.assertRaises(Http404):
            doorcontrol.close_door(request)
