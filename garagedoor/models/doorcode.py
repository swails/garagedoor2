""" Model specifying a code that can be used for entry """
import uuid

import pytz

from django.contrib.auth.hashers import make_password, check_password
from django.db import models

from users.models import User

US_TIMEZONES = pytz.country_timezones('US')

def human_readable_date_string(date):
    """ Human readable date string """
    return date.strftime('%A, %B %-d, %Y')

def human_readable_time_string(time):
    """ Human readable time string """
    return time.strftime('%-I:%M %p')

class DoorCode(models.Model):
    """ A door code """
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    passcode = models.CharField(max_length=128, null=True)
    name = models.CharField(max_length=1024, null=True)
    timezone = models.CharField(max_length=64, null=False, default='America/New_York',
                                choices=zip(US_TIMEZONES, US_TIMEZONES))

    def set_passcode(self, code):
        """
        Sets a hash of the passcode. Since DoorCode is not intended to be populated by the user
        that generates it (it's a code populated by the person to whom they want to grant entry),
        there's no need to have a custom manager to create a code with a pre-existing (and
        pre-hashed) code
        """
        self.passcode = make_password(code)

    def check_passcode(self, code):
        """ Checks the given code to see if it matches. Returns True if the passcode matches,
        False otherwise
        """
        return check_password(code, self.passcode)

class DoorCodeUsed(models.Model):
    """ A record of a door code being used """
    code = models.ForeignKey(DoorCode, on_delete=models.CASCADE)
    time = models.DateTimeField(auto_now_add=True)
    state = models.CharField(null=True, max_length=16)
    valid = models.BooleanField(null=False, default=False)
    reason = models.CharField(
        null=True, max_length=32,
        choices=[('Bad Code', 'Entered code was invalid'),
                 ('Not Active', 'Code was not active when entered'),
                 ('Internal Error', 'Something went wrong connecting to MyQ')]
    )

class DoorCodeActiveDate(models.Model):
    """ A specific day or set of days when this code is active """
    code = models.ForeignKey(DoorCode, on_delete=models.CASCADE)
    start_date = models.DateField(null=False)
    repeat = models.IntegerField(null=True)
    end_date = models.DateField(null=True)
    start_time = models.TimeField(null=True)
    end_time = models.TimeField(null=True)

    @property
    def date_range_string(self):
        """ Formatted string for a date range """
        if self.end_date is None:
            return '{0:s} \u2014'.format(human_readable_date_string(self.start_date))
        return '{0:s} \u2014 {1:s}'.format(human_readable_date_string(self.start_date),
                                           human_readable_date_string(self.end_date))

    @property
    def time_range_string(self):
        """ Formatted string for time range """
        if self.start_time is None or self.end_time is None:
            return 'All Day'
        return '{} \u2014 {}'.format(human_readable_time_string(self.start_time),
                                     human_readable_time_string(self.end_time))

    @property
    def repeat_string(self):
        """ Formatted string to inform how many days between active states """
        return '' if self.repeat is None else '{:d} days'.format(self.repeat)
