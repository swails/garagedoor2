""" Garage door application for dealing with codes and opening/closing door """
from django.apps import AppConfig


class GaragedoorConfig(AppConfig):
    """ Garage door app configuration """
    name = 'garagedoor'
