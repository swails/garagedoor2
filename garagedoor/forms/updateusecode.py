""" Form classes for updating or using a code """
from django import forms

class UserUpdateDoorCodeForm(forms.Form):
    """ Form for updating a code by owner

    Parameters
    ----------
    start_date : datetime
        The day this code should start
    repeat : int
        The number of days between when this code is active
    end_date : int
        When this code should end
    start_time : int
        The time this code should start being active in a day
    end_time : int
        The time this code should stop being active in a day
    """
    start_date = forms.DateField(required=True, widget=forms.SelectDateWidget)
    repeat = forms.IntegerField(min_value=1, required=False, label="Repeat (days)")
    end_date = forms.DateField(required=False, widget=forms.SelectDateWidget)
    start_time = forms.TimeField(required=False, widget=forms.TimeInput)
    end_time = forms.TimeField(required=False, widget=forms.TimeInput)
