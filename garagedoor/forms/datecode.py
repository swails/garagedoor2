""" Form for making and modifying a date code """

from django import forms

from ..models.doorcode import US_TIMEZONES

class CreateDateCodeForm(forms.Form):
    """ A form for creating and editing the date codes """
    name = forms.CharField(max_length=1024)
    time_zone = forms.ChoiceField(required=True, choices=zip(US_TIMEZONES, US_TIMEZONES))
