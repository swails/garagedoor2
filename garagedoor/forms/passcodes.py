""" Forms for "anonymous" users to interact with forms """
from django import forms

class CreateNewPasscodeForm(forms.Form):
    """ Form to create a new passcode for the non-owner

    Parameters
    ----------
    passcode : str
        The password the invited user wants to use
    confirm_passcode : str
        Must match passcode to pass form validation
    """
    passcode = forms.CharField(max_length=128, widget=forms.PasswordInput, required=True)
    confirm_passcode = forms.CharField(max_length=128, widget=forms.PasswordInput, required=True)

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data['passcode'] != cleaned_data['confirm_passcode']:
            raise forms.ValidationError('Passcodes do not match')
        return cleaned_data

class SubmitPasscodeForm(forms.Form):
    """ Form for submitting a passcode to attempt to open the door

    Parameters
    ----------
    passcode : str
        The password to try to use to open the door
    """
    passcode = forms.CharField(max_length=128, widget=forms.PasswordInput, required=True)
