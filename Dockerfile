FROM python:3.7.2-stretch

RUN apt-get update && \
    apt-get -y upgrade && \
    apt-get install -y nginx-full

RUN mkdir -p /var/www/garagedoor2/static && \
    mkdir /garagedoor2

COPY nginx-site /etc/nginx/sites-available/default
COPY . /garagedoor2/

WORKDIR /garagedoor2

RUN pip install -r requirements.txt

RUN python manage.py collectstatic

ENTRYPOINT ["./start_site.sh"]
